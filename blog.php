<?php

	require_once 'include/BlogController.php';
	session_start();
	
	/**
	* Classe permettant d'appeler les bonnes méthode de blogcontroller
	*/
	class Blog{
		
		/**
		* Méthode appelant la bonne méthode en fonction de l'action passée en paramètre 
		*/
		public function chooseAction($action){
			$b = new BlogController();
			switch($action){
				case 'detail' : 
					$b->detailAction();
					break;
				
				case 'cat' : 
					$b->categorieAction();
					break;
					
				case 'user' : 
					$b->profile();
					break;
					
				case 'addComment' :
					$b->addComment();
					break;
														
				default :
					$b->listeAction();
					break;
			}
		}
	}
	
	$blog = new Blog();
	$blog->chooseAction($_REQUEST['action']);

?>
