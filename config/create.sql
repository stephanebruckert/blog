-- phpMyAdmin SQL Dump
-- version 3.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 01, 2012 at 07:35 AM
-- Server version: 5.1.44
-- PHP Version: 5.3.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `BLOG`
--
CREATE DATABASE `BLOG` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `BLOG`;

-- --------------------------------------------------------

--
-- Table structure for table `blog_billets`
--

CREATE TABLE `blog_billets` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `titre` varchar(64) DEFAULT NULL,
  `auteur` int(5) DEFAULT NULL,
  `body` text,
  `cat_id` int(11) DEFAULT '1',
  `date` datetime DEFAULT NULL,
  `datemodif` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 DATA DIRECTORY='./blog/' INDEX DIRECTORY='./blog/' AUTO_INCREMENT=158 ;

--
-- Dumping data for table `blog_billets`
--

INSERT INTO `blog_billets` VALUES(157, 'Penn State Basketball: Tim Frazier Can''t Will Nittany Lions To V', 63, 'Tim Fraizer scored 17 of the Nittany Lionsâ€™ 23 second half points as Penn State fell 52-46 to Wisconsin Tuesday night.\r\nThe game opened quickly as the teams combined to make 7 of their first 11 shots but shooting touch soon left and the game became a defensive battle. Penn State was able to use a 10-0 run to open up an 18-9 lead with 3:51 remaining in the first half. Wisconsin closed the lead with a layup at the buzzer to 23-17.\r\nWisconsinâ€™s 17 points marked the sixth time this season the Nittany Lions held their opponent to 23 or fewer points in the first half.\r\nThe Badgers opened up the second half with a flurry of points going on a 13-3 run to take a 30-26 lead with 12:20 left in the game. Jermaine Marshall, who led Penn State with 10 first half points was held to only three points in the final 20 minutes.\r\nâ€œThey made shots we missed shots,â€ Chambers said. â€œThe game of basketball is mistakes. Whoever makes the most mistakes is going to lose. We probably made some mistakes during those stretches.â€\r\nWisconsin stretched their lead to 7 with a little over eight minutes remaining. Despite several efforts to swing the momentum in their favor, the Nittany Lions were unable to regain the lead even while being down only two with 3:36 left in the game.\r\nJordan Taylorâ€™s three-pointer with 1:44 remaining extended Wisconsinâ€™s lead to 43-38 and put the game out of reach for a Nittany Lion squad that struggled to find clutch scoring. Taylor, who had been bottled up for most of the game still managed 18 points on the night scoring six points in the final minute from the free throw line. \r\nPenn State falls to (10-13, 2-8) on the year. The Nittany Lions are back in action this Saturday at 3 P.M when theyâ€™ll take on the Iowa Hawkeyes in Iowa City. That game can be seen on ESPNU.\r\nWisconsin Coach Bo Ryan on Joe Paterno:\r\nFollowing an early morning session at the Bryce Jordan Center, Wisconsin coach Bo Ryan took his team to the Joe Paterno statue to pay their respects. The team left behind a signed hat and a note from Ryan that read "To the greatest teacher ever." When asked about Paterno, these were Ryan''s remarks.\r\n"Why wouldn''t you (visit the statue)? I would have played for the guy, I would have coached with the guy." Ryan said.\r\n"I asked (Joe) why he didn''t recruit me," Ryan said. "He asked me, ''Well how many games did your high school team in Chester win?'', We were a basketball school but I played football," Ryan said. "I told him ''one game in three years''. Sipping on his soda Joe looked at me and said, ''Well maybe that''s why I didn''t recruit you.''"\r\n"I liked his honesty," Ryan said. "He was right to the point."\r\nNotes: \r\nBilly Oliver dressed for the game but did not play. A possible sign that he is recovering well from concussion like symptoms which have plagued his career while at Penn State.\r\nPat Chambers was dressed in khaki pants and black Nikes for the game in honor of Joe Paterno and Coaches Versus Cancer. \r\nRelated Coverage:', 207, '2012-02-01 07:28:48', NULL);
INSERT INTO `blog_billets` VALUES(151, 'Bet on Tennis: Azarenka wins the Australian Open', 56, 'Victoria Azarenka, famous with many fans that bet on tennis, defeated Maria Sharapova 6-3 6-0, winning the Australian Open.\r\n\r\nFans that bet on tennis saw that in 82 minutes, the Belarusian player was able to overpower the 2008 champion, lifting the Daphne Akhurst Memorial Cup and turning into the fourth womenâ€™s first-time grand slam winner in a row, and many fans were thrilled by what the player was able to accomplish.\r\n\r\nFollowing a nervous start, Azarenka,  22-years-old, who makes great tennis odds, dropped her opening service game and was down 2-0 before she won 12 of the next 13 games, taking the match away from Sharapova, a three-time major winner, and earning a top spot in the rankings.\r\n\r\nâ€œWow,â€ Azarenka said when she first spoke to the crowd, then started to giggle. She thanked her support team, saying â€œYou made me realise I can believe in myself and I can finally raise this trophy.\r\n\r\nâ€œBelarusian people have a mentality of really hard-working people. You can see outside the capitals, big cities, how neat and clean the areas are. So I think that speaks a lot.\r\n\r\nâ€œItâ€™s just amazing to see how much people work, and we are here playing tennis and sometimes complaining about little things. Itâ€™s a little bit silly. Itâ€™s really interesting to see a different kind of life.â€\r\n\r\nAzarenka spoke about her grandmother â€œwho inspires me most in my lifeâ€, before adding: â€œI canâ€™t wait to be back next year, Iâ€™ve had an amazing month in Australia. Itâ€™s a dream true.\r\n\r\nâ€œIâ€™m proud of what Iâ€™m doing. I just want to keep going and keep raising that level.â€\r\n\r\nWhen she was asked whether she felt she that played the perfect match, Azarenka said: â€œI donâ€™t think so. Just the perfect ending.\r\n\r\nPerfect or not, fans that bet on tennis saw an exceptional match.  \r\n\r\nâ€œI didnâ€™t think going into the match I had to play the perfect match. I just had to play better than my opponent.\r\n\r\nAzarenka turns into the first Belarusian grand slam champion, going one better than Natasha Zvereva, who finished runner-up at the 1988â€™s French Open , representing the USSR, and many fans that bet on tennis remember this very well.\r\n\r\nThe player is also the first woman to win in Melbourne both the junior girls and womenâ€™s singles titles since Australiaâ€™s Chris Oâ€™Neil did it in 1978, making great tennis odds along the way.', 207, '2012-02-01 07:22:22', NULL);
INSERT INTO `blog_billets` VALUES(152, 'Severe weather alert as snow and freezing temperatures forecast ', 56, 'The Met Office upgraded its cold weather alert to Level 3, stating that there was a 100 per cent probability of ''''severe'''' conditions across most of England this week.\r\nThe warning means there is expected to be an impact on people''s health and on health services, with a surge in demand for care.\r\nAccording to the Met Office temperatures will drop to as low as -6C (21.2F) tomorrow and on Thursday, when daytime maximums will be no more than 3C (37.4F).\r\nSevere weather warnings for ice were also issued for last night and this morning across eastern parts of England and Scotland, and Northern Ireland, south-west England and south Wales.\r\nA high pressure system hanging over Scandinavia and western Russia is pushing raw, easterly winds towards the UK, meaning this will be the longest spell of cold weather so far this winter, experts say.', 210, '2012-02-01 07:22:47', NULL);
INSERT INTO `blog_billets` VALUES(153, 'The Met Office upgraded its cold weather alert to Level 3, stati', 56, 'It is a clichÃ© almost as old as the motor car itself, and the subject of many a sexist joke.\r\nBut the idea that women cannot park is simply untrue, according to research indicating that female drivers are more adept than men at manoeuvring into a space.\r\nCovert surveillance of car parks across Britain has shown that while women may take longer to park, they are more likely to leave their vehicles in the middle of a bay.\r\nThe study is one of the most comprehensive ever conducted on gender driving differences, and took into account seven key components of parking styles.\r\nWomen were also found to be better at finding spaces, more accurate in lining themselves up before starting each manoeuvre, and more likely to adopt instructorsâ€™ preferred method of reversing into bays.', 209, '2012-02-01 07:23:49', NULL);
INSERT INTO `blog_billets` VALUES(154, 'Bucsâ€™ ice hockey team aiming for winning record  ', 56, 'The Red Bank Regional High School Bucs have turned things around in recent weeks after opening their hockey season with three consecutive losses. With only one winning season in the programâ€™s history, this yearâ€™s team is determined to fight their way to a winning record.\r\n\r\nThe program may very well be at a turning point, considering the roster features four seniors and 10 juniors. While the team is largely inexperienced, head coach Tim Hall believed even before the season started that his team was ready to take the hockey program one step further.\r\n\r\nâ€œWe are inexperienced historically, so our players donâ€™t have a natural aptitude to fight back,â€ said Hall, who is in his second season as head coach.\r\n\r\nHall has seen his team make improvements on the ice, noting tie scores against both Old Bridge (8-2-4) and St. Rose (15-3-1). Red Bank was better prepared for its game against St. Rose on Jan. 18, having lost to the same team nearly a week earlier. Senior goalie Charlie Fox kept Red Bank in the game despite the Bucsâ€™ being heavily outshot, and Red Bank succeeded in its effort to contain St. Roseâ€™s strong front line in a 1-1 tie. Junior forward Nick DeSalvo scored the lone goal.\r\n\r\nâ€œWe did a really good job of being aware of where they were on the ice. We stuck with the game plan,â€ Hall said.\r\n\r\nWhat Hall described as a highlight of the season, the strong performance against St. Rose, was followed by what he deemed a â€œlowlight,â€ a 7-2 loss to Marlboro (11-6-0) on Jan. 21. â€œIt stalled our momentum,â€ he said of the disappointing loss.\r\n\r\nAlthough he said the Bucs have exceeded expectations, the head coach also expected this type of give-and-take season, given the programâ€™s lack of winning experience. Consistency was a concern, but senior defensemen Jack Lynch, Sam Marascio and Mike Wagner, in addition to Fox, have provided the Bucs with veteran experience this season.\r\n\r\nRed Bank also has seen the development of some younger players, including junior defenseman Pat Schroll, who is having a â€œbreakout season,â€ Hall said. â€œPat has made tremendous strides handling the puck. He is becoming a complete player.â€\r\n\r\nBut injuries have been an added challenge for the Bucs, with the team dealing with significant losses to the roster. Junior wingman Josh Suboyu (concussion) is likely done for the season, while junior center Andre Rjedkin (concussion) is readying for a return to the ice. Sophomore defenseman Drue Amato missed time due to an illness.\r\n\r\n\r\nâ€œIf we had all of our horses, we would have been much stronger,â€ Hall said. â€œWeâ€™ve outperformed expectations considering the injuries, but going .500 is never anybodyâ€™s goal.â€\r\n\r\nRed Bank is looking for more â€œrelative successes,â€ Hall said, after bouncing back from a rough start to the season. The early turnaround could give the team greater confidence going into the final weeks of the season, when Red Bank will face Toms River South, Jackson Liberty and Point Pleasant Borough, among others.\r\n\r\nThe Bucs are 6-7-2 overall and 4-4-1 in the Shore Conference D Division.\r\n\r\nâ€œItâ€™s been a good season and a positive experience so far,â€ Hall added.\r\n', 207, '2012-02-01 07:24:37', NULL);
INSERT INTO `blog_billets` VALUES(155, 'Freezing conditions in prospect - in Finland and Eastern Europe ', 56, 'The winter that was very late arriving - at least late in showing up in the south of Finland - now seems to be well and truly settled in.\r\n      On the night between Saturday and Sunday, the temperatures registered in these parts hit a new low for the current winter. The mercury dipped to -35.3Â°C in Taivalkoski in the eastern Koillismaa region, but this record lasted only 24 hours, as in Kuhmo this morning it dipped to -35.4Â°C. Lapland was chilly, too, but a good deal less so than eastern districts of the country.\r\n      This may be as deep as the freeze gets for a little while, as after Monday the most severe frosts are expected to abate, but the Finnish Meteorological Institute (FMI) estimates that the weather will continue to be cold and mainly dry in Finland throughout the current week.\r\n      \r\nA high pressure area over Northwestern Russia will see to it that the weather remains decidedly wintry not only in Finland but also in Eastern Europe and beyond.\r\n      The FMI anticipates the temperatures for example in Poland and Romania will drop down to almost -10.0Â°C.\r\n      \r\nMoreover, at least in the regions of Kainuu and Koillismaa, residents are advised to prepare for intense cold within the next few days.\r\n      Low-lying clouds will drive away the coldest weather from Lapland and Western Finland.\r\n      Every winter, the lowest temperatures in Finland reach at least -35Â°C.\r\n      The current figures, while they may be low enough for the Institute to issue a warning on its website, are still a good way from the all-time one-day record of -51.5Â°C recorded in KittilÃ¤ in Western Lapland in 1999, or the January average temperature record of -29.7Â°C experienced in Kuusamo in 1985. ', 210, '2012-02-01 07:25:13', NULL);
INSERT INTO `blog_billets` VALUES(156, 'Mancini takes rap after City unlocked by Gibson ', 56, '\r\nThe defences put in place by Everton have always seemed one of the more unbreachable obstacles on Manchester City''s journey to world domination and never were the wirecutters required more than last night.\r\n\r\nThey were fished out for the game''s most bizarre moment â€“ a fan handcuffing himself to a goalpost â€“ but otherwise they were missing. Three shots on goal all night and the lead on Manchester United reduced to mere goal difference. Hacking through to the silverware cabinet suddenly looks an uncomfortable task for the side who so recently inflicted grievous damage on all who stood before them.\r\n\r\nRoberto Mancini, the City manager, appeared to know what he was up against before the match, which, considering Everton''s David Moyes had already defeated him four times in five, was no great surprise. "They fight for every ball. It is not enough to have good players," he said of Everton. This made the Italian''s decision to blame himself for the defeat, late last night, all the more eye-catching. "I acknowledge my mistake. It is important for me to know this because like this I can''t do the same mistakes in the next game," Mancini said. Pressed to elaborate, he continued: "I didn''t prepare well for this game. I thought it was maybe easier. I know here it is never easy, never. I didn''t [prepare well] during the training in the last three or four days. The players put everything on the pitch but I made some mistakes."\r\n\r\nThis self-flagellation is not entirely new for Mancini, who also left Merseyside taking the blame after the desperate 3-0 defeat at Anfield last April, five days before an FA Cup semi-final with Manchester United. There was a feeling, after that game, that this had been a ruse to deflect attention from his players and last night certainly presented none of City''s recent excuses to hide behind: no injuries, no fixture logjam and no officiating controversy. The league leaders were simply not up to the kind of industrial struggle which they can expect to dictate the course of their push for the Premier League title over the next 15 games.\r\n\r\nMoyes was delighted. "That''s as good as it has been at Everton for a long time," he said. "With the players that we have missing, it says a lot about the club. We always seem to come back."\r\n\r\nHe said before the sides'' meeting in Manchester last autumn that he felt like a combatant going into "a gunfight armed with a knife" and this did not feel too much different, even though yesterday''s Â£6m addition from Rangers, the Croatian Nikica Jelavic, had arrived fresh from a local hotel room, scrubbed up to be presented as hopefully the biggest gift Glasgow''s blue half has bestowed on Gwladys Street since Duncan Ferguson, 18 years previously. Louis Saha, the player with more shots on and off target than any of Moyes''s players this season, was in the throes of being released to Tottenham Hotspur, with the six months remaining on his contract ripped up, while Moyes'' didn''t exactly look full of defensive ammo either. City''s Edin Dzeko and his stand-in shadow Tony Hibbert were a comical mismatch in size â€“ while the average age of the Everton bench was not much more than 19 years. So much for Everton''s fabled "grand old team".\r\n\r\nThe first half crackled with class differential, even though the best chance arrived when Denis Stracqualursi stooped to meet the Royston Drenthe cross which looped over City''s box after five minutes. It required Joleon Lescott to head off the line. Drenthe''s badly misplaced pass seemed to have undermined everything when David Silva seized on it and launched a 30-yard pass which Sergio Aguero raked back away from Hibbert, firing narrowly wide. Then Dzeko laid into the path of Samir Nasri, whose swerving right-foot shot from 30 yards thudded against the bar. Everton''s greatest attacking intent came when Drenthe, Tim Cahill and Phil Neville applied themselves to the pitch invader who stuck himself to Joe Hart''s left post at the Park End for four minutes.\r\n\r\nBut Everton''s esprit de corps was always there, in the high fives when Marouane Fellaini, who commanded an absorbing midfield battle with Nasri, volleyed narrowly wide the header Cahill laid into his path â€“ then in the counter-attack which sent them ahead. Fitting, indeed, that it should have been the first Everton goal from Darron Gibson â€“ a player gathered up by Moyes, the ultimate alchemist-manager, after Old Trafford deemed him inadequate this month for the battle of Manchester''s gladiators. Aguero''s drive through to goal had just been stopped when the ball was cleared up to Drenthe, who raced through City''s vacant midfield and found Landon Donovan. Gibson stepped into the American''s lay-off with a rasping shot, deflected in off Lescott, resonant of his United best. The shake of Mancini''s head at the space that all three Everton players had been allowed, revealed that he did consider this to be his fault.\r\n\r\nGibson, who still looks like he is adapting at Everton, was granted space to unfurl another volley 10 minutes later. City reasonably felt they should have had a penalty 15 minutes from time when the substitute Aleksandar Kolarov crossed hard into the crook of Neville''s arm. But Everton might have scored on another counter-attack and the course of the night was set. Moyes was impatient to leave his press conference to seal a deal to bring in Steven Pienaar on loan. Mancini was just impatient to leave.\r\n', 207, '2012-02-01 07:26:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blog_categorie`
--

CREATE TABLE `blog_categorie` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `titre` varchar(64) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 DATA DIRECTORY='./blog/' INDEX DIRECTORY='./blog/' AUTO_INCREMENT=212 ;

--
-- Dumping data for table `blog_categorie`
--

INSERT INTO `blog_categorie` VALUES(210, 'Weather', 'Weather is the state of the atmosphere, to the degree that it is hot or cold, wet or dry, calm or stormy, clear or cloudy.[1] Most weather phenomena occur in the troposphere,[2][3] just below the stratosphere. Weather refers, generally, to day-to-day temperature and precipitation activity, whereas climate is the term for the average atmospheric conditions over longer periods of time.[4] When used without qualification, "weather" is understood to be the weather of Earth.');
INSERT INTO `blog_categorie` VALUES(211, 'IT / Internet', 'Information technology (IT) is concerned with technology to treat information. The acquisition, processing, storage and dissemination of vocal, pictorial, textual and numerical information by a microelectronics-based combination of computing and telecommunications are its main fields.[1] The term in its modern sense first appeared in a 1958 article published in the Harvard Business Review, in which authors Leavitt and Whisler commented that "the new technology does not yet have a single established name. We shall call it information technology (IT).".[2] Some of the modern and emerging fields of Information technology are next generation web technologies, bioinformatics, cloud computing, global information systems, large scale knowledgebases, etc. Advancements are mainly driven in the field of computer science.');
INSERT INTO `blog_categorie` VALUES(209, 'Cars', 'An automobile, autocar, motor car or car is a wheeled motor vehicle used for transporting passengers, which also carries its own engine or motor. Most definitions of the term specify that automobiles are designed to run primarily on roads, to have seating for one to eight people, to typically have four wheels, and to be constructed principally for the transport of people rather than goods.[3]');
INSERT INTO `blog_categorie` VALUES(207, 'Sport', 'Sport is all forms of physical activity which, through casual or organised participation, aim to use, maintain or improve physical fitness and provide entertainment to participants. Sport may be competitive, where a winner or winners can be identified by objective means, and may require a degree of skill, especially at higher levels.');
INSERT INTO `blog_categorie` VALUES(208, 'Education', 'Education in its broadest, general sense is the means through which the aims and habits of a group of people lives on from one generation to the next.[1] Generally, it occurs through any experience that has a formative effect on the way one thinks, feels, or acts. In its narrow, technical sense, education is the formal process by which society deliberately transmits its accumulated knowledge, skills, customs and values from one generation to another, e.g., instruction in schools.');

-- --------------------------------------------------------

--
-- Table structure for table `blog_comment`
--

CREATE TABLE `blog_comment` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `body` varchar(255) DEFAULT NULL,
  `user` varchar(15) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `article` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 DATA DIRECTORY='./blog/' INDEX DIRECTORY='./blog/' AUTO_INCREMENT=98 ;

--
-- Dumping data for table `blog_comment`
--

INSERT INTO `blog_comment` VALUES(1, 'zaefazef', 'zefzef', '2012-01-30 07:44:14', 84);
INSERT INTO `blog_comment` VALUES(2, 'gezrgerg', 'uihiuh', '2012-01-30 07:53:31', 84);
INSERT INTO `blog_comment` VALUES(3, 'ezfazef', 'ezfazef', '2012-01-30 09:07:51', 85);
INSERT INTO `blog_comment` VALUES(94, 'Interesting.', 'Stephane', '2012-02-01 07:27:43', 152);
INSERT INTO `blog_comment` VALUES(90, 'Very good article.', 'Admin', '2012-01-31 18:26:39', 104);
INSERT INTO `blog_comment` VALUES(91, 'opjpoj', 'Your nameguh', '2012-02-01 04:48:55', 145);
INSERT INTO `blog_comment` VALUES(92, 'Uncredible! ', 'Stephane', '2012-02-01 07:26:46', 155);
INSERT INTO `blog_comment` VALUES(93, 'I like this sport!', 'Stephane', '2012-02-01 07:27:02', 154);
INSERT INTO `blog_comment` VALUES(82, 'zefzef', 'Your zef', '2012-01-31 04:31:27', 96);
INSERT INTO `blog_comment` VALUES(81, 'zaefzeaf', 'Your zef', '2012-01-31 04:31:25', 96);
INSERT INTO `blog_comment` VALUES(80, 'pijojijo', 'Admin', '2012-01-31 04:23:46', 98);
INSERT INTO `blog_comment` VALUES(79, 'oijioj', 'Admin', '2012-01-31 04:23:44', 98);
INSERT INTO `blog_comment` VALUES(75, 'ezfzef', 'Admin', '2012-01-31 01:30:42', 90);
INSERT INTO `blog_comment` VALUES(72, 'zefzefzef', 'Admin', '2012-01-31 01:29:05', 90);
INSERT INTO `blog_comment` VALUES(78, 'ojoij', 'Admin', '2012-01-31 04:23:42', 98);
INSERT INTO `blog_comment` VALUES(71, 'zefzfe', 'Admin', '2012-01-31 01:29:02', 90);
INSERT INTO `blog_comment` VALUES(69, 'oihjo', 'Admin', '2012-01-30 22:45:01', 90);
INSERT INTO `blog_comment` VALUES(97, 'I know!', 'Stephane', '2012-02-01 07:30:02', 155);
INSERT INTO `blog_comment` VALUES(96, 'I see you like basketball!', 'Stephane', '2012-02-01 07:29:46', 157);
INSERT INTO `blog_comment` VALUES(95, 'It''s too cold for me!', 'Can', '2012-02-01 07:27:59', 155);
INSERT INTO `blog_comment` VALUES(68, 'zeafezafazef', 'Admin', '2012-01-30 12:49:29', 88);
INSERT INTO `blog_comment` VALUES(67, 'azefazefazef', 'Admin', '2012-01-30 12:49:19', 88);
INSERT INTO `blog_comment` VALUES(66, 'azefezfazef', 'Admin', '2012-01-30 12:37:52', 79);
INSERT INTO `blog_comment` VALUES(65, 'zeafzaef', 'azefazef', '2012-01-30 12:37:43', 79);
INSERT INTO `blog_comment` VALUES(64, 'azefazef', 'Your nameazefaz', '2012-01-30 12:37:41', 79);
INSERT INTO `blog_comment` VALUES(63, 'azefazef', 'Admin', '2012-01-30 12:37:32', 79);
INSERT INTO `blog_comment` VALUES(56, 'zaefazefzeafazef', 'azefzaef', '2012-01-30 11:09:16', 85);
INSERT INTO `blog_comment` VALUES(57, 'zaeazefazefzeafazefzeaf', 'azfazef', '2012-01-30 11:09:19', 85);
INSERT INTO `blog_comment` VALUES(58, 'zef', 'zef', '2012-01-30 11:49:37', 86);
INSERT INTO `blog_comment` VALUES(59, 'zefzef', 'Admin', '2012-01-30 11:49:48', 87);
INSERT INTO `blog_comment` VALUES(60, 'ezfzefzef', 'Admin', '2012-01-30 11:50:21', 87);
INSERT INTO `blog_comment` VALUES(61, 'ijoij', 'Admin', '2012-01-30 12:01:34', 87);

-- --------------------------------------------------------

--
-- Table structure for table `blog_users`
--

CREATE TABLE `blog_users` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `login` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `mail` varchar(64) DEFAULT NULL,
  `level` int(1) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `activity` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 DATA DIRECTORY='./blog/' INDEX DIRECTORY='./blog/' AUTO_INCREMENT=66 ;

--
-- Dumping data for table `blog_users`
--

INSERT INTO `blog_users` VALUES(64, 'Stephane', 'c320dc92c374906fbc071bb485150871', 'stephane@mail.fr', 3, NULL, '2012-02-01 07:18:12', NULL);
INSERT INTO `blog_users` VALUES(65, 'Writer', 'bbad3d307be077bacf6e77b0520db65f', 'writer@writer.zaf', 1, NULL, '2012-02-01 07:18:30', NULL);
INSERT INTO `blog_users` VALUES(56, 'Admin', 'e3afed0047b08059d0fada10f400c1e5', 'Admin@admin.com', 3, NULL, '2011-01-17 05:36:58', NULL);
INSERT INTO `blog_users` VALUES(63, 'Can', 'd677b410373bc790d9a8ec2e68090839', 'can@mail.tk', 3, NULL, '2012-02-01 07:17:40', NULL);
INSERT INTO `blog_users` VALUES(62, 'Moderator', '1a1dc91c907325c69271ddf0c944bc72', 'Mail', 2, NULL, '2012-01-31 04:55:53', NULL);
