<?php

	require_once 'include/AdminController.php';
	session_start();
	
	/**
	* Classe permettant la gestion des billets et des cat�gories dans le blog
	*/
	class Admin{
		/**
		* M�thode permettant de choisir l'action � effectuer par le GestionController
		*/
		public function chooseAction($action){
			$g = new AdminController();
			if ($_SESSION['authentification'] == 'correcte'){		
				switch($action){
					case 'adminbillet' : 
						$g->formulaireBillet();
						break;
						
					case 'admincategorie' : 
						$g->formulaireCategorie();
						break;
												
					case 'addM' : 
						$g->ajoutBillet();
						break;
						
					case 'addC' : 
						$g->ajoutCategorie();
						break;
						
					case 'addMbr' : 
						$g->ajoutUtilisateur();
						break;
					
					case 'removeM' : 
						$g->suppressionBillet();
						break;
						
					case 'removeC' : 
						$g->suppressionCategorie();
						break;

					case 'removeU' : 
						$g->suppressionUtilisateur();
						break;
						
					case 'formModifyM' :
						$g->formulaireModificationBillet();
						break;
					
					case 'modifyM' : 
						$g->modificationBillet();
						break;
					
					case 'formModifyC' :
						$g->formulaireModificationCategorie();
						break;
					
					case 'modifyC' : 
						$g->modificationCategorie();
						break;
						
					case 'logout' : 
						$g->deconnexion();
						break;
						
					case 'adminmember' : 
						$g->gestionMembres();
						break;

					case 'removeComment' :
						$g->removeComment();
						break;
						
					default:
						$g->administration();
						break;
				}
			}else{
				if ($action == 'login'){
					$g->login();
				}else{
					$a = new Affichage();
					$categories = Categorie::findAll();
					$billets = Billet::findAll();
					
					$res = '<div class=error><b>VOUS N\'ÊTES PAS CONNECTÉ</b></div>';
					$a->affichePage($a->listeBillets($billets),$a->listeTitresCategories($categories),$a->listeTitresBillets($billets), $res);
				}
			}
		}
	}
	
	$admin = new Admin();
	$admin->chooseAction($_REQUEST['action']);

?>
