<?php
/**
 *  La Classe Categorie  realise un Active Record sur la table blog_categorie
 */
class Categorie{
	/**
		*  Identifiant de categorie
		*  @access private
		*  @var integer
		*/
	private $id ;

	/**
		*  libelle de categorie
		*  @access private
		*  @var String
		*/
	private $titre;

	/**
		*  description de categorie
		*  @access private
		*  @var String
		*/
	private $description;

	/**
		*  Constructeur de Categorie
		*  fabrique une nouvelle categorie vide
		*/
	public function __construct(){
	}

	/**
		*  Fonction Magic retournant une chaine de caracteres imprimable pour imprimer facilement un Ouvrage
		*  @return String
		*/
	public function __toString(){
		return "[". __CLASS__ . "] id : ". $this->id . ": titre  ". $this->titre  .": description ". $this->description;
	}

	/**
		*   fonction d'acces aux attributs d'un objet.
		*   Recoit en parametre le nom de l'attribut accede et retourne sa valeur.
		*   @param String $attr_name attribute name
		*   @return mixed
		*/
	public function getAttr($attr_name) {
		if (property_exists( __CLASS__, $attr_name)){
			return $this->$attr_name;
		}
		$emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
		throw new Exception($emess, 45);
	}

	/**
		*   fonction de modification des attributs d'un objet.
		*   Recoit en parametre le nom de l'attribut modifie et la nouvelle valeur
		*   @param String $attr_name attribute name
		*   @param mixed $attr_val attribute value
		*   @return mixed new attribute value
		*/
	public function setAttr($attr_name, $attr_val){
		if (property_exists( __CLASS__, $attr_name)){
			$this->$attr_name=$attr_val;
			return $this->$attr_name;
		}
		$emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
		throw new Exception($emess, 45);
	}

	/**
		*   fonction qui enregistre l'etat de l'objet dans la table
		*   Si l'objet possede un identifiant : mise � jour de l aligne correspondante
		*   sinon : insertion dans une nouvelle ligne
		*   @return int le nombre de lignes touchees
		*/
	public function save(){
		if (!isset($this->id)){
			return $this->insert();
		}else{
			return $this->update();
		}
	}

	/**
		*   Sauvegarde l'objet courant dans la base en faisant un update
		*   l'identifiant de l'objet doit exister (insert obligatoire auparavant)
		*   @return int nombre de lignes mises � jour
		*/
	public function update(){
		if (!isset($this->id)){
			throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
		}
		$save_query = "update blog_categorie set titre=".(isset($this->titre) ? "'$this->titre'" : "null").",description=".(isset($this->description) ? "'$this->description'" : "null")."where id=$this->id";
		$c = Base::getConnection();
		$q = mysql_query($save_query,$c);
		if (! $q){
			throw new Exception('Mysql query error: '. $save_query . ' : ' . mysql_error() );
		}
		return mysql_affected_rows($c);

	}


	/**
		*   fonction qui supprime la ligne dans la table corrsepondant � l'objet courant
		*   L'objet doit poss�der un OID
		*/
	public function delete(){
		if (isset($this->id)){
			$delete_query = 'delete from blog_categorie where id = '.$this->id;
			$c = Base::getConnection();
			$q = mysql_query($delete_query,$c);
			if (! $q){
				throw new Exception('Mysql query error: '. $delete_query . ' : ' . mysql_error() );
			}
			return mysql_affected_rows();
		}
	}


	/**
		*   Insertion dans la base
		*
		*   Ins�re l'objet comme une nouvelle ligne dans la table
		*   l'objet doit poss�der  un code_rayon
		*
		*   @return int nombre de lignes ins�r�es
		*/
	public function insert(){
		$insert_query = "insert into blog_categorie values(null,".(isset($this->titre) ? "'$this->titre'" : "null").",".(isset($this->description) ? "'$this->description'" : "null").")";
		$c = Base::getConnection();
		$q = mysql_query($insert_query,$c);

		if (! $q){
			throw new Exception('Mysql query error: '. $insert_query . ' : ' . mysql_error() );
		}
		$this->id = mysql_insert_id();
			
		return mysql_affected_rows();
	}


	/**
		*   Finder sur ID
		*
		*   Retrouve la ligne de la table correspondant au ID pass� en param�tre,
		*   retourne un objet
		*
		*   @static
		*   @param integer $id OID to find
		*   @return Categorie renvoie un objet de type Categorie
		*/
	public static function findById($id) {
		$query = "select * from blog_categorie where id=". " $id ";
		$c = Base::getConnection();
		$dbres = mysql_query($query,$c);
		if (! $dbres){
			throw new Exception('Mysql query error: '. $query . ' : ' . mysql_error() );
		}
		$row = mysql_fetch_assoc($dbres);
		$cat = new Categorie();
		$cat->setAttr('id',$row['id']);
		$cat->setAttr('titre',$row['titre']);
		$cat->setAttr('description',$row['description']);
		return $cat;
	}

	/**
		*   Finder sur Titre
		*
		*   Retrouve la ligne de la table correspondant au titre pass� en param�tre,
		*   retourne un objet
		*
		*   @static
		*   @param integer $titre Titre to find
		*   @return Categorie renvoie un objet de type Categorie
		*/
	public static function findByTitre($titre) {
		$query = "select * from blog_categorie where id=". " '$titre' ";
		$c = Base::getConnection();
		$dbres = mysql_query($query,$c);
		if (! $dbres){
			throw new Exception('Mysql query error: '. $query . ' : ' . mysql_error() );
		}
		$row = mysql_fetch_assoc($dbres);
		$cat = new Categorie();
		$cat->setAttr('id',$row['id']);
		$cat->setAttr('titre',$row['titre']);
		$cat->setAttr('description',$row['description']);
		return $cat;
	}

	/**
		*   Finder All
		*
		*   Renvoie toutes les lignes de la table categorie
		*   sous la forme d'un tableau d'objet
		*
		*   @static
		*   @return Array renvoie un tableau de categorie
		*/
	public static function findAll() {
		$query = "select * from blog_categorie";
		$c = Base::getConnection();
		$dbres = mysql_query($query,$c);
		if (! $dbres){
			throw new Exception('Mysql query error: '. $query . ' : ' . mysql_error() );
		}
			
		$tab = array();
		while($row = mysql_fetch_array($dbres)){
			$cat = new Categorie();
			$cat->setAttr('id',$row['id']);
			$cat->setAttr('titre',$row['titre']);
			$cat->setAttr('description',$row['description']);
			$tab[]=$cat;
		}
		return $tab;
	}


	/**
		*   Trouve les cat�gories de chacun des billets donn�s en param�tre
		*
		*   Renvoie toutes les lignes de la table users
		*   sous la forme d'un tableau d'objet
		*
		*   @param  Array re�oit un tableau de billets.
		*   @static
		*   @return Array renvoie un tableau de users
		*/
	public static function findCategs($billets) {
		$tab = array();
		foreach ($billets as $billet){
			$tab[] = Categorie::findById($billet->getAttr('cat_id'));
		}
		return $tab;
	}
}

?>