<?php
require_once 'Base.php';
require_once 'Billet.php';
require_once 'Categorie.php';
require_once 'Utilisateur.php';
require_once 'Comment.php';

/**
 * Classe permettant l'affichage des différents composants du blog
 */
class Affichage{
	/**
		* Méthode pour afficher tout une page,
		* Elle prend en paramètre le bloc du centre, le menu de gauche et celui de droite
		*/
	public function affichePage($content, $menuleft, $menuright, $res) {
		echo '<!doctype html>
<meta charset="UTF-8">
<title>Stéphane Bruckert</title>
<link rel="stylesheet" type="text/css" href="css/sober.css"/>
<link rel="stylesheet" type="text/css" href="css/sobercss3.css"/>
<script src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>

	<!--[if IE]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->
<body>
<div id="outwrapper">
  <div id="wrapper">
    <div id="innerwrapper">
      <section id="top_bar"> </section>
      <header><img src="img/sbs.png" alt="sbs"/></header>
      <nav>
        <ul class="ulmenu">
        	<li ' . ((!isset($_GET['action'])) ? 'id="selected"' : '') . ' class="limenu"><a href="blog.php">HOME & NEWS (' . sizeof(Billet::findAll()) . ')</a></li>';
		$categories = Categorie::findAll();

		$sorted_data = array();
		foreach ($categories as $categorie) {
			$size = sizeof(Billet::findByCat($categorie->getAttr('id')));
			$sorted_data[$categorie->getAttr('id')] = $size;
		}
		arsort($sorted_data);

		$i = 0;
		foreach ($sorted_data as $key => $value) {
			$categorie = Categorie::findById($key);
			if ($_GET['action'] == cat && $_GET['id'] == $key)
			$selected = 'id="selected"';
			else
			$selected = '';

			echo '<li ' . $selected . ' class="limenu"><a href="blog.php?action=cat&id=' .$categorie->getAttr('id') . '">' . $categorie->getAttr('titre') . ' (' . $value . ')</a></li>';

			$i++;
			if ($i == 4) break;
		}

		echo 	'</ul>
      </nav>
      ' . $res . '
      <aside>
        <section class="block" style="padding: 20px 10px;">
          <div class="title">
		  <h1>Login » </h1>
		</div>
		'. Affichage::formulaireLogin()
		. $menuleft . $menuright . '
        </section>
      </aside>
      <section id="content" class="vertibar">
        ' . $content . '
      </section>
      <footer>
        <p>Copyright  © 2012 - Code & Design by Stéphane Bruckert & Ethem Can Karaoğuz</p>
        <p>Software used: Dreamweaver, Photoshop.<br />
          Tested on: Firefox, Chrome, IE6, Netscape, Safari<br />
          <br />
          <a href="http://validator.w3.org/check?uri=http%3A%2F%2Fusers.metropolia.fi%2F~stephanb%2Fproject%2Findex.html&amp;charset=%28detect+automatically%29&amp;doctype=Inline&amp;group=0" target="_blank"><img
      src="http://kentie.net/valid-html5.png" alt="Valid XHTML 1.0 Strict" height="31" width="88" /></a> <a href="http://validator.w3.org/check?uri=http%3A%2F%2Fusers.metropolia.fi%2F~stephanb%2Fproject%2Fsober.css"> <img style="border:0;width:88px;height:31px"
            src="http://jigsaw.w3.org/css-validator/images/vcss"
            alt="Valid CSS!" /> </a></p>
        <p style="border:0px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eget risus sit amet nisl malesuada vestibulum. Aliquam erat volutpat. Donec nec leo metus. Aliquam nec pharetra nisi. Curabitur tempus, neque sed fringilla adipiscing, metus dui auctor massa, vitae elementum magna ligula sit amet est. Suspendisse accumsan tortor eget ante sodales id tincidunt nisi auctor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam pulvinar posuere consequat.</p>
      </footer>
    </div>
  </div>
</div>
</body>
';
	}

	/**
		* Méthode permettant d'afficher le billet passé en paramètre
		*/
	public function unBillet($billet){
			
		$categ = Categorie::findById($billet->getAttr('cat_id'));
		$user = Utilisateur::findById($billet->getAttr('auteur'));
		$comments = Comment::findByArticle($billet->getAttr('id'));
			
		$html_comments = "<br />";
			
		foreach ($comments as $comment) {
			$html_comments .= "<div class='comment'>The ".substr($comment->getAttr('date'),8,2).
	'/' .substr($comment->getAttr('date'),5,2). '/' .substr($comment->getAttr('date'),0,4).
	' at '.substr($comment->getAttr('date'),10,20). " - " . $comment->getAttr('user') . " said (<a class='removeCom' id=" . $comment->getAttr('id') . ">Remove</a>):<br />" . $comment->getAttr('body') . "</div>";
		}
			
		$html_comments .= '
						<form style="margin: 3px 0px 0px 150px; border: 1px solid white; background-color: gainsboro; padding: 6px;" id="addComment" action="blog.php?action=addComment&ajax=1" method="post">
						' . ($_SESSION['authentification'] ? $_SESSION['name']. ':' : '') . '<input type="' . (($_SESSION['authentification'] == "correcte") ? "hidden" : "text") . '" value="' . (($_SESSION['authentification'] == "correcte") ? $_SESSION['name'] : 'Your name') . '" name="user" id ="user"/><br />
						<input type="hidden" value="' . $billet->getAttr('id') . '" name="article"/>
						<textarea name="body" id="body" rows="2" cols="50">Your comment</textarea>
						<br /><input type="submit" name="add_comment" value="Say!">
						</form>';
			
		return 	'<div class="title"><h1><a href="blog.php?action=cat&id=' .$categ->getAttr('id').'">'.$categ->getAttr('titre'). '</a> à
					' .$billet->getAttr('titre'). '</h1></div>' .
					'<article class="news" id="' .$billet->getAttr('id'). '">
						<p>' .$billet->getAttr('body').'</p>
						<div class="footer">
						<em>Posté par <a href="blog.php?action=user&id=' .$user->getAttr('id'). '">' .$user->getAttr('login'). '</a> le '.substr($billet->getAttr('date'),8,2). 
						'/' .substr($billet->getAttr('date'),5,2). '/' .substr($billet->getAttr('date'),0,4).
						' à '.substr($billet->getAttr('date'),10,20).'</em> - <a class="deleteArticle" href="admin.php?action=removeM&id=' . $billet->getAttr('id') . '">Delete</a>
						</div>
						'. $html_comments .'
					</article>
					
					<script>
					$(document).ready(function() {
						$("#addComment").ajaxForm(function showResponse(responseText, statusText, xhr, $form) {
							 var body = $("textarea[name=body]").val();
						     $("#addComment").before("<div class=\'comment\'>You just said (<a class=\'removeCom\' id=" + responseText + ">Remove</a>):<br />" + body + "</div>");
						     $("textarea[name=body]").val("");
						});
					
						$("section#content").delegate(".removeCom", "click", function(e) {
							var id = $(this).attr(\'id\');
							that = $(this).parent();
							$.ajax({
						      type: "get",
						      url: "admin.php?action=removeComment",
						      data: "ajax=1&id=" + id,
						      success: function() {
						      	that.remove();
						      }
						    });
						});
						
						var text = document.getElementById(\'user\');
						  text.addEventListener(\'focus\', function(e) {
						  	if (e.target.value == "Your name") {
						   		e.target.value = "";
						   	}
						  }, true);
						  text.addEventListener(\'blur\', function(e) {
						  	if (e.target.value == "") {
						    	e.target.value = "Your name";
						    }
						  }, true);
						  
						var text = document.getElementById(\'body\');
						  text.addEventListener(\'focus\', function(e) {
						  	if (e.target.innerHTML == "Your comment") {
						   		e.target.innerHTML = "";
						   	}
						  }, true);
						  text.addEventListener(\'blur\', function(e) {
						  	if (e.target.innerHTML == "") {
						    	e.target.innerHTMLss = "Your comment";
						    }
						  }, true);
					});
					</script>';
	}

	/**
		* Méthode permettant d'afficher une liste de billets passée en paramètre
		*/
	public function listeBillets($billets){
		$ret = '';
			
		if ($_GET['action'] != "cat") {
			$ret .= '<div class="intro">
			
			          <h2>Welcome! This is our blog...</h2>
			          <p>Duis posuere iaculis lacus vitae tincidunt. Ut nec ipsum diam. Curabitur viverra turpis vel sem sollicitudin ac tempus neque feugiat. Nulla semper elementum ligula non auctor. Vivamus venenatis justo eget mi facilisis varius. Morbi laoreet quam ac nunc lacinia dignissim. </p>
			          <div id="signature">Stéphane Bruckert & Ethem Can Karaoğuz</div>
			        </div>';
		} else {
			$categorie = Categorie::findById($_GET['id']);

			$ret .=
					'<div class="intro">
			          <h2>This is all about ' . $categorie->getAttr('titre'). '</h2>
			          <p>' . $categorie->getAttr('description'). '</p>
			        </div>';
		}
		
		if (sizeof($billets) == 0) {
			$ret .= "<center><b>There are no articles in this category</b></center>";
		} else {
			
			foreach($billets as $billet) {
				$suite = '';
	
				if (strlen($billet->getAttr('body')) > 300){
					$suite = '... <a rel="plus" href="blog.php?action=detail&id=' .$billet->getAttr('id'). '"><strong>Read more</strong></a>';
				}
	
				$categ = Categorie::findById($billet->getAttr('cat_id'));
				$user = Utilisateur::findById($billet->getAttr('auteur'));
				$comments = Comment::findByArticle($billet->getAttr('id'));
	
				$ret .= '
						<div class="title">
						<h1>
							<a href="blog.php?action=cat&id=' .$categ->getAttr('id') .'">'.$categ->getAttr('titre'). '</a> » 
							<a href="blog.php?action=detail&id=' .$billet->getAttr('id'). '">'.$billet->getAttr('titre').'</a>
						</h1>
						</div>
						<article class="news" id="' .$billet->getAttr('id'). '">
						<p>' .substr($billet->getAttr('body'),0,300) . ' ' . $suite . '</p>
						<div class="footer">' .
							'<em>Posté par <a id="3" href="blog.php?action=user&id=' .$user->getAttr('id'). '">' .$user->getAttr('login'). '</a> le '.substr($billet->getAttr('date'),8,2). 
							'/' .substr($billet->getAttr('date'),5,2). '/' .substr($billet->getAttr('date'),0,4).
							' à '.substr($billet->getAttr('date'),10,20).
							'</em>
							 - <a href="blog.php?action=detail&id=' .$billet->getAttr('id'). '">'. ((sizeof($comments) > 0) ? 
							 "See ". sizeof($comments) ." comment(s)" : "Be the firt to comment").'</a>
							 ' . (($_SESSION['authentification'] == "correcte") ? (' - <a class="deleteArticle" href="admin.php?action=removeM&id=' . $billet->getAttr('id') . '">Delete</a>') : "" ) . '
						</div>
						</article>';
			}
		}

		return $ret .
				'<script>
				$("section#content").delegate("a[rel=plus]", "click", function(e) {
				    e.preventDefault();
				    parent = $(this).parent();
				    var id = parent.parent().attr(\'id\');
				    
				    $.ajax({
				      type: "get",
				      url: "blog.php?action=detail",
				      data: "ajax=1&id=" + id,
				      beforeSend: function() {
				        parent.children("a").remove();
				        parent.append(" <img src=\"img/loader1.gif\" alt=\"loader\" />");
				      },
				      success: function(data) {
				      	if (data != "") {
				      		parent.empty();
				      		parent.append(data);
				      	}
				      }
				    });
				    return false;
				  });
				  $("section#content").delegate("a.deleteArticle", "click", function(e) {
					parent = $(this).parent().parent();
				    var id = parent.attr(\'id\');
				    
				    $.ajax({
				      type: "get",
				      url: "admin.php?action=removeM",
				      data: "ajax=1&id=" + id,
				      success: function(res) {
			      			parent.prev().remove();
			    			parent.remove();
				      }
				    });
				    
					return false;
				  });
				</script>';
	}

	/**
		* Méthode permettant d'afficher la liste des catégories
		*/
	public function listeTitresCategories($categories){
		$ret .='
			<div class="title">
			    <h1>ALL Categories » </h1>
			</div>';
			
		foreach($categories as $cat){
			$ret .= '<div class=liste>';
			if (strlen($cat->getAttr('titre')) > 23){
				$suite = '...';
			}
			$ret .= '<a href="blog.php?action=cat&id=' .$cat->getAttr('id'). '">' .substr($cat->getAttr('titre'),0,23). $suite . '</a>';
			if ($_SESSION['authentification'] == 'correcte'){
				$ret .= ' <a href="admin.php?action=removeC&id=' .$cat->getAttr('id'). '"><img src="img/remove.png" alt="remove"/></a> ';
			}
			$ret .= '</div>';
		}
		return $ret;
	}

	/**
		* Méthode permettant d'afficher la liste des titres des billets
		*/
	public function listeTitresBillets($billets){
		
	}

	/**
		* Méthode permettant d'afficher la liste des titres des billets
		*/
	public function listeTitresAdmin(){
		return 	'<a href="admin.php?action=adminbillet"><div class="liste">Article management</div></a>'.
					'<a href="admin.php?action=admincategorie"><div class="liste">Categorie management</div></a>'.
					'<a href="admin.php?action=adminmember"><div class="liste">User management</div></a>';		}

		/**
		 * Méthode permettant d'afficher les formulaires de gestion des cat�gories
		 */
		public function formulaireCategorie($categories){
			$ret = '<form method="post" action="admin.php?action=addC">
						<fieldset>
							<legend>Add a categorie</legend>
							<input type="text" value="Title" name="titre"/>
							<input type="text" value="Description" name="description"/>	
							<input type="submit" name="add_categorie" value="Ajouter">	
						</fieldset>
					</form><br/>
					';
			$ret .= '<TABLE class="linear">';
			$ret .= '<TR>';
			$ret .= '<TH> Title </TH>';
			$ret .= '<TH> Description </TH>';
			$ret .= '<th> </th>';
			$ret .= '<th> </th>';
			$ret .= '</TR>';
				
			$i = 0;
			while($i < sizeof($categories)) {
				$bg = ($i % 2 == 0) ? ' style="background-color:#FFFFFF;"' : '';
				$ret .= '<TR'. $bg .'>';
				$ret .= '<TD> '.$categories[$i]->getAttr('titre').' </TD>';
				$ret .= '<TD> '.$categories[$i]->getAttr('description').' </TD>';
				$ret .= '<TD><center><a href="admin.php?action=formModifyC&id='.$categories[$i]->getAttr('id').'"><img src="img/edit.png" /></a></center></TD>';
				$ret .= '<TD><center><a href="admin.php?action=removeC&last=admin&id='.$categories[$i]->getAttr('id').'"><img src="img/remove.png" /></a></center></TD>';
				$ret .= '</TR>';
				$i++;
			}
			$ret .= '</TABLE>';
				
			return $ret;
		}

		/**
		 * M�thode permettant d'afficher les formulaires de gestion des billets
		 */
		public function formulaireBillet($billets,$categories){
			$ret = '<form id="editArticle" method="post" action="admin.php?action=addM">
						<fieldset>
							<legend>Add or edit an article</legend>
							Title: <input name="titre" type="text" /> Category : <select name="cat_id">';

			foreach($categories as $categ){
				$ret .= '<option value="' .$categ->getAttr('id'). '">' .$categ->getAttr('titre'). '</option>';
			}
			$ret .= '</select>
					
							<br /><br />Content :<br /> <textarea name="body" rows="15" cols="90"></textarea>
							<br />
							<input style="float:right;" type="submit" name="add_billet" value="OK">
						</fieldset>
					</form><br/>
					';
			$ret .= '<TABLE class="linear">';
			$ret .= '<TR>';
			$ret .= '<TH> Title </TH>';
			$ret .= '<TH> Author </TH>';
			$ret .= '<th> </th>';
			$ret .= '<th> </th>';
			$ret .= '</TR>';
				
			$i = 0;
			while($i < sizeof($billets)) {
				$bg = ($i % 2 == 0) ? ' style="background-color:#FFFFFF;"' : '';
				$ret .= '<TR'. $bg .'>';
				$ret .= '<TD> '.$billets[$i]->getAttr('titre').' </TD>';
				$ret .= '<TD> '. Utilisateur::findById($billets[$i]->getAttr('auteur'))->getAttr('login').' </TD>';
				$ret .= '<TD><center><a id='.$billets[$i]->getAttr('id').' class="edit" href="admin.php?action=formModifyM&id='.$billets[$i]->getAttr('id').'"><img src="img/edit.png" /></a></center></TD>';
				$ret .= '<TD><center><a id='.$billets[$i]->getAttr('id').' class="rem" href="admin.php?action=removeM&last=adminbillet&id='.$billets[$i]->getAttr('id').'"><img src="img/remove.png" /></a></center></TD>';
				$ret .= '</TR>';
				$i++;
			}
			$ret .= '</TABLE>';
				
			return $ret;
		}

		/**
		 * Méthode permettant d'afficher pour modification les formulaires de gestion des billets
		 */
		public function formulaireBilletModifier($billet,$categories){
			$ret = '<form method="post" action="admin.php?action=modifyM">
						<fieldset>
							<legend>Modification d\'un billet</legend>
							<p>Titre : <br/><input name="titre" type="text" value="'. $billet->getAttr('titre').'" /></p>
												
							<p>Catégorie : <br/><select name="cat_id">';
			foreach($categories as $categ){
				$ret .= '<option value="' .$categ->getAttr('id'). '">' .$categ->getAttr('titre'). '</option>';
			}
			$ret .= '</select></p>
					
							<p>Corps : <br/><textarea name="body" rows="15" cols="50">'.$billet->getAttr('body').'</textarea></p>
							
							<p><input type="hidden" name="id" value="'.$billet->getAttr('id').'">
							<input type="submit" name="modify_billet" value="Modifier"></p>
						</fieldset>
					</form><br/>';
			return $ret;
		}


		/**
		 * M�thode permettant d'afficher pour modification les formulaires de gestion des categories
		 */
		public function formulaireCategorieModifier($categ){
			$ret = '<form method="post" action="admin.php?action=modifyC">
						<fieldset>
							<legend>Modification d\'une catégorie</legend>
							<p>Titre : <br/><input name="titre" type="text" value="'. $categ->getAttr('titre').'" /></p>					
							<p>Description : <br/><input name="description" type="text" value="'.$categ->getAttr('description').'" /></p>
							
							<p><input type="hidden" name="id" value="'.$categ->getAttr('id').'">
							<input type="submit" name="modify_categorie" value="Modifier"></p>
						</fieldset>
					</form><br/>';
			return $ret;
		}

		/**
		 * Méthode permettant d'afficher le formulaire de login
		 */
		public function formulaireLogin(){
			if ($_SESSION['authentification'] == 'correcte'){
				$ret .= 'Hello <a href="blog.php?action=user&id='. $_SESSION['id'].'"><strong>' . $_SESSION['name'] . '</strong></a> (';

				$ret .= '<a href=admin.php?action=logout><strong>Logout</strong></a>)';

				$ret .= '<br /><br /><a href="admin.php?action=admin">Administration (';
				for ($i = 1; $i <= $_SESSION['level']; $i++)
				$ret .= '<img src="img/star.png">';
				$ret .= ')</a><br /><br />';
				$ret .= Affichage::listeTitresAdmin();

			}else{
				$ret .= '<form method="post" action="admin.php?action=login">
						<input type="text" value="Pseudo" name="login" id="login"/>
						<input type="password" value="Password" name="password" id="password" onclick="this.value=\'\';"/>
						<input type="submit" name="add_categorie" value="Login">
						</form>';
			}
			return $ret . '
						<script>
						var text = document.getElementById(\'login\');
						  text.addEventListener(\'focus\', function(e) {
						  	if (e.target.value == "Pseudo") {
						   		e.target.value = "";
						   	}
						  }, true);
						
						  text.addEventListener(\'blur\', function(e) {
						  	if (e.target.value == "") {
						    	e.target.value = "Pseudo";
						    }
						  }, true);
						  
						</script>';
		}

		public function ficheUtilisateur($user) {
			$ret .= 'Identifiant : '.$user->getAttr('login');
			$ret .= '<br />Adresse électronique : '.$user->getAttr('mail');
			$ret .= '<br />Droits d\'administration : ';
			for ($i = 1; $i <= $user->getAttr('level'); $i++) {
				$ret .= '<img src="img/star.png">';
			}
			if ($user->getAttr('avatar') != '') {
				$ret .= '<br />Avatar : <img src="'.$user->getAttr('avatar').'">';
			}

			return $ret;
		}
			

		public function gestionMembres($utilisateurs) {
			if ($_SESSION['level'] == 3) {

				$ret .= '<form method="post" action="admin.php?action=addMbr">
							<fieldset><legend>Add a user</legend>
							<input type="text" value="Pseudo" onclick="this.value=\'\';" name="login"/>
							<input type="password" value="Password" onclick="this.value=\'\';" name="password"/>
							<input type="text" value="Mail" onclick="this.value=\'\';" name="mail"/>
							<SELECT name="level">
								<OPTION VALUE="1">Writer</OPTION>
								<OPTION VALUE="2">Moderator</OPTION>
								<OPTION VALUE="3">Administrator</OPTION>
							</SELECT>
							<input type="submit" name="add_member" value="Créer">
							</fieldset>
						</form>';
				$ret .= '<br /><TABLE class="linear">';
				$ret .= '<TR>';
				$ret .= '<TH> Pseudonym </TH>';
				$ret .= '<TH> Mail </TH>';
				$ret .= '<TH> Joined </TH>';
				$ret .= '<TH> Level </TH>';
				$ret .= '<TH> Date </TH>';
				$ret .= '<TH></TH>';
				$ret .= '</TR>';

				$i = 0;
				while($i < sizeof($utilisateurs)) {
					$bg = ($i % 2 == 0) ? ' style="background-color:#FFFFFF;"' : '';
					$ret .= '<TR'. $bg .'>';
					$ret .= '<TD> '.$utilisateurs[$i]->getAttr('login').' </TD>';
					$ret .= '<TD> '.$utilisateurs[$i]->getAttr('mail').' </TD>';
					$ret .= '<TD> '.$utilisateurs[$i]->getAttr('date').'</TD>';
					$ret .= '<TD><center>';
					for ($stars = 1; $stars <= $utilisateurs[$i]->getAttr('level'); $stars++) {
						$ret .= '<img src="img/star.png">';
					}
					$ret .= '</center></TD>';
					$ret .= '<TD> </TD>';
					$ret .= '<TD><center><a href="admin.php?action=removeU&id='.$utilisateurs[$i]->getAttr('id').'"><img src="img/remove.png" /></a></center></TD>';
					$ret .= '</TR>';
					$i++;
				}
				$ret .= '</TABLE>';
			}
			return $ret;
		}

		public function preferences() {
			$ret .= 'Welcome into the administration';
			return $ret;
		}

		public function actuality() {
			return '
			          <div class="title">
			            <h1>Last comments » </h1>
			          </div>
			          <ul class="puce" id="maj">
			            <li><a href="media.html">Added 3 pictures.<small>Hier</small></a></li>
			            <li><a href="cv.html">Updated his experience.<small>3 days ago</small></a></li>
			            <li><a href="cv.html">Added a language.<small>One week ago</small></a></li>
			            <li><a href="index.html">Creation of the website<small>2 weeks ago</small></a></li>
			          </ul>
			          <div class="title">
			            <h1>Top categories » </h1>
			          </div>
			          <ul class="puce" id="maj">
			            <li><a href="media.html">Added 3 pictures.<small>Hier</small></a></li>
			            <li><a href="cv.html">Updated his experience.<small>3 days ago</small></a></li>
			            <li><a href="cv.html">Added a language.<small>One week ago</small></a></li>
			            <li><a href="index.html">Creation of the website<small>2 weeks ago</small></a></li>
			          </ul>';
		}
}
?>
