<?php
	/**
	* Classe permettant la connexion � la base de donn�es
	*/
	class Base{
		/**
		* Connexion � la base
		*/
		private static $connection;
		
		/**
		* M�thode permettant de se connecter � une base de donn�e
		* La base de donn�e cible est donn�e dans le fichier config.php
		*/
		private static function connect(){
			require("./config/config.php");
			self::$connection = mysql_connect($host,$user,$password);
			mysql_select_db($base,self::$connection);
		}
		
		/**
		* M�thode permettant d'obtenir la connexion � la base
		* si elle existe on la retourne
		* sinon on la cr��e et on la retourne
		* @return la connexion
		*/
		public static function getConnection(){			
			if (!(isset(self::$connection))){
				Base::connect();
			}
			return self::$connection;
		}
	}
?>
