<?php
	/**
	*  La Classe Utilisateur realise un Active Record sur la table blog_users
	*/
	class Utilisateur{
		/**
		*  ID de l'utilisateur
		*  @access private
		*  @var String
		*/
		private $id;
		
		/**
		*  login de l'utilisateur
		*  @access private
		*  @var String
		*/
		private $login;
		
		/**
		*  mot de passe de l'utilisateur
		*  @access private
		*  @var String
		*/
		private $password;
	
		/**
		*  mail de l'utilisateur
		*  @access private
		*  @var String
		*/
		private $mail;
		
		/**
		*  niveau d'administration de l'utilisateur
		*  @access private
		*  @var Integer
		*/
		private $level;
		
		/**
		*  Avatar de l'utilisateur
		*  @access private
		*  @var Integer
		*/
		private $avatar;
		
		/**
		*  Date de cr�ation de l'utilisateur
		*  @access private
		*  @var String
		*/
		private $date;

		/**
		*  Date de la derni�re activit� de l'utilisateur
		*  @access private
		*  @var String
		*/
		private $activity;
		
		/**
		*  Constructeur de Utilisateur
		*  fabrique un nouveau billet vide
		*/
		public function __construct(){
			$this->date = date('Y-m-d H:i:s', time());
		}
		
		/**
		*  Fonction Magic retournant une chaine de caract�res imprimable pour imprimer facilement un Ouvrage
		*  @return String
		*/
		public function __toString(){
			return "[". __CLASS__ . "] id : ". $this->id . ": login  ". $this->login  .": password ". $this->password .": mail ". $this->mail .": level ".$this->level .": avatar ".$this->avatar .": date ".$this->date;
		}
		
		/**
		*   fonction d'acces aux attributs d'un objet.
		*   Recoit en parametre le nom de l'attribut accede et retourne sa valeur.
		*   @param String $attr_name attribute name 
		*   @return mixed
		*/
		public function getAttr($attr_name) {
			if (property_exists( __CLASS__, $attr_name)){ 
				return $this->$attr_name;
			} 
			$emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
			throw new Exception($emess, 45);
		}
		
		/**
		*   Fonction de modification des attributs d'un objet.
		*   Recoit en parametre le nom de l'attribut modifie et la nouvelle valeur
		*   @param String $attr_name attribute name 
		*   @param mixed $attr_val attribute value
		*   @return mixed new attribute value
		*/
		public function setAttr($attr_name, $attr_val){
			if (property_exists( __CLASS__, $attr_name)){
				$this->$attr_name=$attr_val; 
				return $this->$attr_name;
			} 
			$emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
			throw new Exception($emess, 45);
		}

		/**
		*   fonction qui enregistre l'etat de l'objet dans la table
		*   Si l'objet possede un identifiant : mise � jour de l aligne correspondante
		*   sinon : insertion dans une nouvelle ligne
		*   @return int le nombre de lignes touchees
		*/
		public function save(){
			if (!isset($this->login)){
				return $this->insert();
			}else{
				return $this->update();
			}
		}	
		
		/**
		*   Sauvegarde l'objet courant dans la base en faisant un update
		*   l'identifiant de l'objet doit exister (insert obligatoire auparavant)
		*   @return int nombre de lignes mises � jour
		*/
		public function update(){
			if (!isset($this->login)){
				throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
			} 
			$save_query = "update blog_users set password=".(isset($this->password) ? "'$this->password'" : "null").
							",mail=".(isset($this->mail) ? "'$this->mail'" : "null").
							",level=".(isset($this->level) ? "'$this->level'" : "null").
							",avatar=".(isset($this->avatar) ? "'$this->avatar'" : "null").
							"where login=$this->login";
			$c = Base::getConnection();
			$q = mysql_query($save_query,$c);
			if (! $q){
				throw new Exception('Mysql query error: '. $save_query . ' : ' . mysql_error() );
			}
			return mysql_affected_rows($c);

		}


		/**
		*   fonction qui supprime la ligne dans la table corrsepondant � l'objet courant
		*   L'objet doit poss�der un OID
		*/
		public function delete(){
			if (isset($this->id)){
				$delete_query = 'delete from blog_users where id = '.$this->id;
				$c = Base::getConnection();
				$q = mysql_query($delete_query,$c);
				if (! $q){
					throw new Exception('Mysql query error: '. $delete_query . ' : ' . mysql_error() );
				}
				return mysql_affected_rows();
			}
		}


		/**
		*   Insertion dans la base
		*
		*   Ins�re l'objet comme une nouvelle ligne dans la table
		*   l'objet doit poss�der  un code_rayon
		*
		*   @return int nombre de lignes ins�r�es
		*/									
		public function insert(){
			$password = md5($this->password);
			$insert_query = "insert into blog_users values(null,".(isset($this->login) ? "'$this->login'" : "null").
							",".(isset($this->password) ? "'$password'" : "null").
							",".(isset($this->mail) ? "'$this->mail'" : "null").
							",".(isset($this->level) ? "'$this->level'" : "0").
							",".(isset($this->avatar) ? "'$this->avatar'" : "null").
							",".(isset($this->date) ? "'$this->date'" : "null").
							",".(isset($this->activity) ? "'$this->activity'" : "null").")";
	
			$c = Base::getConnection();
			$q = mysql_query($insert_query,$c);
			if (! $q){
				throw new Exception('Mysql query error: '. $insert_query . ' : ' . mysql_error() );
			}
			$this->id = mysql_insert_id();
			return mysql_affected_rows();
		}


		/**
		*   Retrouve la ligne de la table correspondant au login et password pass�s en param�tre,
		*   retourne un objet
		*  
		*   @static
		*   @param string login et password
		*   @return Utilisateur renvoie un objet de type Utilisaeur
		*/
		public static function find($login, $password) {
			$password = md5($password);
			$query = "select * from blog_users where login='$login' and password='$password'";
			$c = Base::getConnection();
			$dbres = mysql_query($query,$c);
			if (! $dbres){
				throw new Exception('Mysql query error: '. $query . ' : ' . mysql_error() );
			}
			$row = mysql_fetch_assoc($dbres);
			$user = new Utilisateur();
			$user->setAttr('id',$row['ID']);
			$user->setAttr('login',$row['login']);
			$user->setAttr('mail',$row['mail']);
			$user->setAttr('level',$row['level']);
			$user->setAttr('avatar',$row['avatar']);
			$user->setAttr('date',$row['date']);
			$user->setAttr('activity',$row['activity']);
			return $user;
		}
		
		/**
		*   Cherche si login existe d�j�
		*  
		*   @static
		*   @param string login et password
		*   @return Utilisateur renvoie un objet de type Utilisaeur
		*/
		public static function findLogin($login) {			
			$query = "select * from blog_users where login='$login'";
			$c = Base::getConnection();
			$dbres = mysql_query($query,$c);
			if (! $dbres){
				throw new Exception('Mysql query error: '. $query . ' : ' . mysql_error() );
			}
			$i = 0;
			while($row = mysql_fetch_array($dbres)){
				$i++;
			}
			return $i;
		}
		
		/**
		*   Cherche Utilisateur pour login donn�
		*  
		*   @static
		*   @param string login et password
		*   @return Utilisateur renvoie un objet de type Utilisaeur
		*/
		public static function findById($id) {
			$query .= "select * from blog_users where id='$id'";
			$c = Base::getConnection();
			$dbres = mysql_query($query,$c);
			if (! $dbres){
				throw new Exception('Mysql query error: '. $query . ' : ' . mysql_error() );
			}
			$row = mysql_fetch_assoc($dbres);
			$user = new Utilisateur();
			$user->setAttr('id',$row['ID']);
			$user->setAttr('login',$row['login']);
			$user->setAttr('mail',$row['mail']);
			$user->setAttr('level',$row['level']);
			$user->setAttr('avatar',$row['avatar']);
			$user->setAttr('date',$row['date']);
			$user->setAttr('activity',$row['activity']);
			return $user;
		}

		/**
		*   Finder All
		*
		*   Renvoie toutes les lignes de la table users
		*   sous la forme d'un tableau d'objet
		*  
		*   @static
		*   @return Array renvoie un tableau de users
		*/
		public static function findAll() {
			$query = "select * from blog_users";
			$c = Base::getConnection();
			$dbres = mysql_query($query,$c);
			if (! $dbres){
				throw new Exception('Mysql query error: '. $query . ' : ' . mysql_error() );
			}
			
			
			
			$tab = array();
			while($row = mysql_fetch_array($dbres)){
				$user = new Utilisateur();
				$user->setAttr('id',$row['ID']);
				$user->setAttr('login',$row['login']);
				$user->setAttr('mail',$row['mail']);
				$user->setAttr('level',$row['level']);
				$user->setAttr('avatar',$row['avatar']);
				$user->setAttr('date',$row['date']);
				$user->setAttr('activity',$row['activity']);
				$tab[]=$user;		
			}
			return $tab;
		}
		
		/**
		*   Trouve les cr�ateurs de chacun des billets donn�s en param�tre
		*
		*   Renvoie toutes les lignes de la table users
		*   sous la forme d'un tableau d'objet
		*  
		*   @param  Array re�oit un tableau de billets.
		*   @static
		*   @return Array renvoie un tableau de users
		*/
		public static function findUsers($billets) {
			$tab = array();
			foreach ($billets as $billet){
				$tab[] = Utilisateur::findById($billet->getAttr('auteur'));
			}
			return $tab;
		}

	}

?>
