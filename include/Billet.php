<?php
	/**
	*  La Classe Billet realise un Active Record sur la table blog_billets
	*/
	class Billet{
		/**
		*  Identifiant du billet
		*  @access private
		*  @var integer
		*/
		private $id ;
		
		/**
		*  libelle du billet
		*  @access private
		*  @var String
		*/
		private $titre;
		
		/**
		*  auteur du billet
		*  @access private
		*  @var String
		*/
		private $auteur;
		
		/**
		*  corps du billet
		*  @access private
		*  @var String
		*/
		private $body;
			
		/**
		*  categorie du billet
		*  @access private
		*  @var integer
		*/
		private $cat_id;
		
		/**
		*  date du billet
		*  @access private
		*  @var integer
		*/
		private $date;
		
		/**
		*  date du billet
		*  @access private
		*  @var integer
		*/
		private $datemodif;
		
		/**
		*  Constructeur de Billet
		*  fabrique un nouveau billet vide
		*/
		public function __construct(){
			$this->date = date('Y-m-d H:i:s', time());
		}
		
		/**
		*  Fonction Magic retournant une chaine de caracteres imprimable pour imprimer facilement un Ouvrage
		*  @return String
		*/
		public function __toString(){
			return "[". __CLASS__ . "] id : ". $this->id . ": titre  ". $this->titre  . "auteur: ". $this->auteur . ": contenu ". $this->body .": cat_id ". $this->cat_id .": date ". $this->date .": datemodif ". $this->datemodif;
		}
		
		/**
		*   fonction d'acces aux attributs d'un objet.
		*   Recoit en parametre le nom de l'attribut accede et retourne sa valeur.
		*   @param String $attr_name attribute name 
		*   @return mixed
		*/
		public function getAttr($attr_name) {
			if (property_exists( __CLASS__, $attr_name)){ 
				return $this->$attr_name;
			} 
			$emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
			throw new Exception($emess, 45);
		}
		
		/**
		*   fonction de modification des attributs d'un objet.
		*   Recoit en parametre le nom de l'attribut modifie et la nouvelle valeur
		*   @param String $attr_name attribute name 
		*   @param mixed $attr_val attribute value
		*   @return mixed new attribute value
		*/
		public function setAttr($attr_name, $attr_val){
			if (property_exists( __CLASS__, $attr_name)){
				$this->$attr_name=$attr_val; 
				return $this->$attr_name;
			} 
			$emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
			throw new Exception($emess, 45);
		}

		/**
		*   fonction qui enregistre l'etat de l'objet dans la table
		*   Si l'objet possede un identifiant : mise � jour de l aligne correspondante
		*   sinon : insertion dans une nouvelle ligne
		*   @return int le nombre de lignes touchees
		*/
		public function save(){
			if (!isset($this->id)){
				return $this->insert();
			}else{
				return $this->update();
			}
		}	
		
		/**
		*   Sauvegarde l'objet courant dans la base en faisant un update
		*   l'identifiant de l'objet doit exister (insert obligatoire auparavant)
		*   @return int nombre de lignes mises � jour
		*/
		public function update(){
			if (!isset($this->id)){
				throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
			} 
			$d = date('Y-m-d H:i:s', time());
			$save_query = "update blog_billets set titre=".(isset($this->titre) ? "'$this->titre'" : "null").
							",auteur=".(isset($this->auteur) ? "'$this->auteur'" : "null").
							",body=".(isset($this->body) ? "'$this->body'" : "null").
							",cat_id=".(isset($this->cat_id) ? "'$this->cat_id'" : "null").
							",date=".(isset($this->date) ? "'$this->date'" : "null").
							",datemodif='". $d .
							"' where id=$this->id";
			$c = Base::getConnection();
			$q = mysql_query($save_query,$c);
			if (! $q){
				throw new Exception('Mysql query error: '. $save_query . ' : ' . mysql_error() );
			}
			return mysql_affected_rows($c);

		}


		/**
		*   fonction qui supprime la ligne dans la table corrsepondant � l'objet courant
		*   L'objet doit poss�der un OID
		*/
		public function delete(){
			if (isset($this->id)){
				$delete_query = 'delete from blog_billets where id = '.$this->id;
				$c = Base::getConnection();
				$q = mysql_query($delete_query,$c);
				if (! $q){
					throw new Exception('Mysql query error: '. $delete_query . ' : ' . mysql_error() );
				}
				return mysql_affected_rows();
			}
		}


		/**
		*   Insertion dans la base
		*
		*   Ins�re l'objet comme une nouvelle ligne dans la table
		*   l'objet doit poss�der  un code_rayon
		*
		*   @return int nombre de lignes ins�r�es
		*/									
		public function insert(){
			$insert_query = "insert into blog_billets values(null,".(isset($this->titre) ? "'$this->titre'" : "null").
							",".(isset($this->auteur) ? "'$this->auteur'" : "null").
							",".(isset($this->body) ? "'$this->body'" : "null").
							",".(isset($this->cat_id) ? "'$this->cat_id'" : "null").
							",'$this->date',null)";
			$c = Base::getConnection();
			$q = mysql_query($insert_query,$c);
			if (! $q){
				throw new Exception('Mysql query error: '. $insert_query . ' : ' . mysql_error() );
			}
			$this->id = mysql_insert_id();
			
			return mysql_affected_rows();
		}


		/**
		*   Finder sur ID
		*
		*   Retrouve la ligne de la table correspondant au ID pass� en param�tre,
		*   retourne un objet
		*  
		*   @static
		*   @param integer $id OID to find
		*   @return Billet renvoie un objet de type Billet
		*/
		public static function findById($id) {
			$query = "select * from blog_billets where id=". " $id ";
			$c = Base::getConnection();
			$dbres = mysql_query($query,$c);
			if (! $dbres){
				throw new Exception('Mysql query error: '. $query . ' : ' . mysql_error() );
			}
			$row = mysql_fetch_assoc($dbres);
			$billet = new Billet();
			$billet->setAttr('id',$row['id']);
			$billet->setAttr('titre',$row['titre']);
			$billet->setAttr('auteur',$row['auteur']);
			$billet->setAttr('body',$row['body']);
			$billet->setAttr('cat_id',$row['cat_id']);
			$billet->setAttr('date',$row['date']);
			$billet->setAttr('datemodif',$row['datemodif']);
			return $billet;
		}
		
		/**
		*   Finder sur categorie
		*
		*   Retrouve les lignes de la table correspondant a la cat�gorie pass� en param�tre,
		*   retourne un tableau d'objet
		*  
		*   @static
		*   @param integer $id OID to find
		*   @return Billet renvoie un objet de type Billet
		*/
		public static function findByCat($cat) {
			$query = "select * from blog_billets where cat_id=". " $cat order by date desc";
			$c = Base::getConnection();
			$dbres = mysql_query($query,$c);
			if (! $dbres){
				throw new Exception('Mysql query error: '. $query . ' : ' . mysql_error() );
			}
			$tab = array();
			while($row = mysql_fetch_array($dbres)){
				$billet = new Billet();
				$billet->setAttr('id',$row['id']);
				$billet->setAttr('titre',$row['titre']);
				$billet->setAttr('auteur',$row['auteur']);
				$billet->setAttr('body',$row['body']);
				$billet->setAttr('cat_id',$row['cat_id']);
				$billet->setAttr('date',$row['date']);
				$billet->setAttr('datemodif',$row['datemodif']);
				$tab[]=$billet;
			}
			return $tab;
		}

		/**
		*   Finder All
		*
		*   Renvoie toutes les lignes de la table categorie
		*   sous la forme d'un tableau d'objet
		*  
		*   @static
		*   @return Array renvoie un tableau de categorie
		*/
		public static function findAll() {
			$query = "select * from blog_billets order by date desc";
			$c = Base::getConnection();
			$dbres = mysql_query($query,$c);
			if (! $dbres){
				throw new Exception('Mysql query error: '. $query . ' : ' . mysql_error() );
			}
			
			$tab = array();
			while($row = mysql_fetch_array($dbres)){
				$bil = new Billet();
				$bil->setAttr('id',$row['id']);
				$bil->setAttr('titre',$row['titre']);
				$bil->setAttr('auteur',$row['auteur']);
				$bil->setAttr('body',$row['body']);
				$bil->setAttr('cat_id',$row['cat_id']);
				$bil->setAttr('date',$row['date']);
				$bil->setAttr('datemodif',$row['datemodif']);
				$tab[]=$bil;		
			}
			return $tab;
		}
	}

?>
