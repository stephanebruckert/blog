<?php
	/**
	*  La Classe Billet realise un Active Record sur la table blog_billets
	*/
	class Comment{
		/**
		*  Identifiant du billet
		*  @access private
		*  @var integer
		*/
		private $id ;
		
		/**
		*  libelle du billet
		*  @access private
		*  @var String
		*/
		private $body;
		
		/**
		*  auteur du billet
		*  @access private
		*  @var String
		*/
		private $user;
		
		/**
		*  corps du billet
		*  @access private
		*  @var String
		*/
		private $date;
			
		/**
		*  categorie du billet
		*  @access private
		*  @var integer
		*/
		private $article;
		
		/**
		*  Constructeur de Billet
		*  fabrique un nouveau billet vide
		*/
		public function __construct(){
			$this->date = date('Y-m-d H:i:s', time());
		}
		
		/**
		*  Fonction Magic retournant une chaine de caracteres imprimable pour imprimer facilement un Ouvrage
		*  @return String
		*/
		public function __toString(){
			return "[". __CLASS__ . "] id : ". $this->id . ": body  ". $this->body  . "user: ". $this->user . ": date ". $this->date .": article ". $this->article;
		}
		
		/**
		*   fonction d'acces aux attributs d'un objet.
		*   Recoit en parametre le nom de l'attribut accede et retourne sa valeur.
		*   @param String $attr_name attribute name 
		*   @return mixed
		*/
		public function getAttr($attr_name) {
			if (property_exists( __CLASS__, $attr_name)){ 
				return $this->$attr_name;
			} 
			$emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
			throw new Exception($emess, 45);
		}
		
		/**
		*   fonction de modification des attributs d'un objet.
		*   Recoit en parametre le nom de l'attribut modifie et la nouvelle valeur
		*   @param String $attr_name attribute name 
		*   @param mixed $attr_val attribute value
		*   @return mixed new attribute value
		*/
		public function setAttr($attr_name, $attr_val){
			if (property_exists( __CLASS__, $attr_name)){
				$this->$attr_name=$attr_val; 
				return $this->$attr_name;
			} 
			$emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
			throw new Exception($emess, 45);
		}

		/**
		*   fonction qui enregistre l'etat de l'objet dans la table
		*   Si l'objet possede un identifiant : mise � jour de l aligne correspondante
		*   sinon : insertion dans une nouvelle ligne
		*   @return int le nombre de lignes touchees
		*/
		public function save(){
			if (!isset($this->id)){
				return $this->insert();
			}else{
				return $this->update();
			}
		}	
		
		/**
		*   Sauvegarde l'objet courant dans la base en faisant un update
		*   l'identifiant de l'objet doit exister (insert obligatoire auparavant)
		*   @return int nombre de lignes mises � jour
		*/
		public function update(){
			if (!isset($this->id)){
				throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
			} 
			$d = date('Y-m-d H:i:s', time());
			$save_query = "update blog_comment set body=".(isset($this->body) ? "'$this->body'" : "null").
							",user=".(isset($this->user) ? "'$this->user'" : "null").
							",date='". $d .
							",article=".(isset($this->article) ? "'$this->article'" : "null").
							"' where id=$this->id";
			$c = Base::getConnection();
			$q = mysql_query($save_query,$c);
			if (! $q){
				throw new Exception('Mysql query error: '. $save_query . ' : ' . mysql_error() );
			}
			return mysql_affected_rows($c);

		}


		/**
		*   fonction qui supprime la ligne dans la table corrsepondant � l'objet courant
		*   L'objet doit poss�der un OID
		*/
		public function delete(){
			if (isset($this->id)){
				$delete_query = 'delete from blog_comment where id = '.$this->id;
				$c = Base::getConnection();
				$q = mysql_query($delete_query,$c);
				if (! $q){
					throw new Exception('Mysql query error: '. $delete_query . ' : ' . mysql_error() );
				}
				return mysql_affected_rows();
			}
		}


		/**
		*   Insertion dans la base
		*
		*   Ins�re l'objet comme une nouvelle ligne dans la table
		*   l'objet doit poss�der  un code_rayon
		*
		*   @return int nombre de lignes ins�r�es
		*/									
		public function insert(){
			$insert_query = "insert into blog_comment values(null,".(isset($this->body) ? "'$this->body'" : "null").
							",".(isset($this->user) ? "'$this->user'" : "null").
							",'" . $this->date . "'".
							",".(isset($this->article) ? "'$this->article'" : "null") . ")";
			$c = Base::getConnection();
			$q = mysql_query($insert_query,$c);
			if (! $q){
				throw new Exception('Mysql query error: '. $insert_query . ' : ' . mysql_error() );
			}
			$this->id = mysql_insert_id();
			
			return mysql_affected_rows();
		}


		/**
		*   Finder sur ID
		*
		*   Retrouve la ligne de la table correspondant au ID pass� en param�tre,
		*   retourne un objet
		*  
		*   @static
		*   @param integer $id OID to find
		*   @return Billet renvoie un objet de type Billet
		*/
		public static function findById($id) {
			$query = "select * from blog_comment where id=". " $id ";
			$c = Base::getConnection();
			$dbres = mysql_query($query,$c);
			if (! $dbres){
				throw new Exception('Mysql query error: '. $query . ' : ' . mysql_error() );
			}
			$row = mysql_fetch_assoc($dbres);
			$comment = new comment();
			$comment->setAttr('id',$row['id']);
			$comment->setAttr('body',$row['body']);
			$comment->setAttr('user',$row['user']);
			$comment->setAttr('date',$row['date']);
			$comment->setAttr('article',$row['article']);
			return $comment;
		}
		
		/**
		*   Finder sur categorie
		*
		*   Retrouve les lignes de la table correspondant a la cat�gorie pass� en param�tre,
		*   retourne un tableau d'objet
		*  
		*   @static
		*   @param integer $id OID to find
		*   @return Billet renvoie un objet de type Billet
		*/
		public static function findByArticle($article) {
			$query = "select * from blog_comment where article=$article order by date asc";
			$c = Base::getConnection();
			$dbres = mysql_query($query,$c);
			if (! $dbres){
				throw new Exception('Mysql query error: '. $query . ' : ' . mysql_error() );
			}
			$tab = array();
			while($row = mysql_fetch_array($dbres)){
				$comment = new comment();
				$comment->setAttr('id',$row['id']);
				$comment->setAttr('body',$row['body']);
				$comment->setAttr('user',$row['user']);
				$comment->setAttr('date',$row['date']);
				$comment->setAttr('article',$row['article']);
				$tab[]=$comment;
			}
			return $tab;
		}

		/**
		*   Finder All
		*
		*   Renvoie toutes les lignes de la table categorie
		*   sous la forme d'un tableau d'objet
		*  
		*   @static
		*   @return Array renvoie un tableau de categorie
		*/
		public static function findAll() {
			$query = "select * from blog_comment order by date desc";
			$c = Base::getConnection();
			$dbres = mysql_query($query,$c);
			if (! $dbres){
				throw new Exception('Mysql query error: '. $query . ' : ' . mysql_error() );
			}
			
			$tab = array();
			while($row = mysql_fetch_array($dbres)){
				$comment = new comment();
				$comment->setAttr('id',$row['id']);
				$comment->setAttr('body',$row['body']);
				$comment->setAttr('user',$row['user']);
				$comment->setAttr('date',$row['date']);
				$comment->setAttr('article',$row['article']);
				$tab[]=$comment;		
			}
			return $tab;
		}
	}

?>
