<?php

require_once 'Affichage.php';
require_once 'Utilisateur.php';

/**
 * Controllor allowing generation of the displaying in the administration of the articles and categories
 */
class AdminController{

	/**
	 * Méthode displaying forms for the management of the articles
	 */
	public function formulaireBillet(){
		$a = new Affichage();
		$categories = Categorie::findAll();
		$billets = Billet::findAll();

		$a->affichePage($a->formulaireBillet($billets,$categories),"","");
	}

	/**
	 * Method displaying forms for the management of categories
	 */
	public function formulaireCategorie(){
		$a = new Affichage();
		$categories = Categorie::findAll();
		$billets = Billet::findAll();

		$a->affichePage($a->formulaireCategorie($categories),"","");
	}

	/**
		* Method which add an article which the passed params in the forms
		*/
	public function ajoutBillet(){
		$a = new Affichage();
		$categories = Categorie::findAll();
		$billets = Billet::findAll();
			
		if (($_REQUEST['titre'] != '') && ($_REQUEST['body'] != '') && ($_REQUEST['cat_id'] != '')){
			$billet = new Billet();
			$billet->setAttr('titre',$_REQUEST['titre']);
			$billet->setAttr('auteur',$_SESSION['id']);
			$billet->setAttr('body',$_REQUEST['body']);
			$billet->setAttr('cat_id',$_REQUEST['cat_id']);
			$billet->insert();

			$users = Utilisateur::findUsers($billets);

			$res = '<div class=good><b>ARTICLE ADDED</b></div>';
		}else{
			$res = '<div class=error><b>UNCOMPLETE ARTICLE</b></div>';
		}
		$categories = Categorie::findAll();
		$billets = Billet::findAll();
		$a->affichePage($a->formulaireBillet($billets,$categories),"","",$res);
	}

	/**
		* Method which add a category using variables in params thanks to the forms
		*/
	public function ajoutCategorie(){
		$a = new Affichage();
		$billets = Billet::findAll();
			
		if ($_REQUEST['titre'] != ''){
			$cat = new Categorie();
			$cat->setAttr('titre',$_REQUEST['titre']);
			$cat->setAttr('description',$_REQUEST['description']);
			$cat->insert();

			$users = Utilisateur::findUsers($billets);

			$res = '<div class=good><b>CATEGORY ADDED</b></div>';
		}else{
			$res = '<div class=error><b>UNCOMPLETE CATEGORY</b></div>';
		}
		$categories = Categorie::findAll();
		$a->affichePage($a->formulaireCategorie($categories),"","",$res);
	}


	/**
		* Method which add member using the variables in params thanks to the forms
		*/
	public function ajoutUtilisateur(){
		$a = new Affichage();
		$categories = Categorie::findAll();
		$billets = Billet::findAll();
		$users = Utilisateur::findAll();
		if ($_SESSION['level'] == 3) {
			if (Utilisateur::findLogin($_REQUEST['login']) == '') {
				if (($_REQUEST['login'] != '') && ($_REQUEST['password'] != '')){
					$user = new Utilisateur();
					$user->setAttr('id',$_REQUEST['ID']);
					$user->setAttr('login',$_REQUEST['login']);
					$user->setAttr('password',$_REQUEST['password']);
					$user->setAttr('mail',$_REQUEST['mail']);
					$user->setAttr('level',$_REQUEST['level']);
					$user->insert();
					$res = '<div class=good><b>USER ADDED</b></div>';
				}else{
					$res = '<div class=error><b>UNCOMPLETE USER</b></div>';
				}
			}else{
				$res = '<div class=error><b>USER ALREADY EXISTING</b></div>';
			}
			$users = Utilisateur::findAll();
			$a->affichePage($a->gestionMembres($users),"","",$res);
		} else {
			$res = 'YOU DON\'T HAVE ENOUGH RIGHTS.';
			$a->affichePage($a->preferences(),"","",$res);
		}
	}

	/**
		* Method which delete a category using the variables in params 
		*/
	public function suppressionCategorie(){
		$a = new Affichage();

		if ($_SESSION['level'] > 1) {

			$billets = Billet::findAll();
	
			if ($_REQUEST['id'] != ''){
				$cat = new Categorie();
				$cat->setAttr('id',$_REQUEST['id']);
				$cat->delete();
									
				$categories = Categorie::findAll();
				
				if ($_GET['last'] == "admin") {
					$body = $a->formulaireCategorie($categories);
				} else {
					$body = $a->listeBillets($billets);
				}	
				
				$res = '<div class=good><b>CATEGORY REMOVED</b></div>';
				$a->affichePage($body,$a->listeTitresCategories($categories),$a->listeTitresBillets($billets),$res);
			}else{
				$categories = Categorie::findAll();
				$res = '<div class=error><b>CATEGORY NOT REMOVED</b></div>';
				
				if ($_GET['last'] == "admin") {
					$body = $a->formulaireCategorie($categories);
				} else {
					$body = $a->listeBillets($billets);
				}
				
				$a->affichePage($body,$a->listeTitresCategories($categories),"",$res);
			}
		} else {
			$res = '<div class=error><b>YOU DON\'T HAVE THE NECESSARY RIGHTS.</b></div>';
			$a->affichePage($a->preferences(),"","",$res);
		}
	}

	/**
		* Method which delete an article using variables in params
		*/
	public function suppressionBillet(){
		$a = new Affichage();
		$billet = Billet::findById($_REQUEST['id']);
			
		$categories = Categorie::findAll();
		if ($_SESSION['level'] > 1 || ($_SESSION['id'] == $billet->getAttr('auteur'))) {
			$a = new Affichage();

			if ($_REQUEST['id'] != ''){
				$billet = new Billet();
				$billet->setAttr('id',$_REQUEST['id']);
				$billet->delete();

				$users = Utilisateur::findUsers($billets);
					
				$res = '<div class=good><b>ARTICLE DELETED</b></div>';
			}else{
				$res = '<div class=error><b>ARTICLE NOT DELETED</b></div>';
			}
		} else {
			$res = '<div class=error><b>YOU DON\'T HAVE THE NECESSARY RIGHTS.</b></div>';
		}

		$billets = Billet::findAll();
		if ($_GET['last'] != "adminbillet") {
			$a->affichePage($a->listeBillets($billets),$a->listeTitresCategories($categories),"");
		} else {
			$a->affichePage($a->formulaireBillet($billets,$categories),"","",$res);
		}

	}

	/**
		* Method which delete a user using variables in params
		*/
	public function suppressionUtilisateur(){
		$a = new Affichage();
		$categories = Categorie::findAll();
		$billets = Billet::findAll();
			
		if (($_SESSION['level'] > 2) && ($_SESSION['id'] != $_REQUEST['id'])) {
			if ($_REQUEST['id'] != ''){
				$user = new Utilisateur();
				$user->setAttr('id',$_REQUEST['id']);
				$user->delete();
					
				$res = '<div class=good><b>USER DELETED</b></div>';
			}else{
				$res = '<div class=error><b>USER NOT DELETED</b></div>';
			}
			$users = Utilisateur::findAll();
			$a->affichePage($a->gestionMembres($users),"","",$res);
		} else {
			$res = '<div class=error><b>YOU DON\'T HAVE THE NECESSARY RIGHTS.</b></div>';
			$a->affichePage($a->preferences(),"","",$res);
		}
	}


	/**
		* Method displaying the modification article form
		*/
	public function formulaireModificationBillet(){
		$billet = Billet::findById($_REQUEST['id']);
		if($_GET['ajax'] != 1) {
			$a = new Affichage();
			$categories = Categorie::findAll();
			$billets = Billet::findAll();

			if (($_SESSION['level'] > 1) || ($_SESSION['id'] != $billet->getAttr('auteur'))){
				if ($_REQUEST['id'] != ''){
					$a->affichePage($a->formulaireBilletModifier($billet,$categories),$a->listeTitresCategories($categories),$a->listeTitresBillets($billets));
				}else{
					$res = '<div class=error><b>NO ARTICLE/b></div>';
					$a->affichePage($a->formulaireBillet($billets,$categories),$a->listeTitresCategories($categories),$a->listeTitresBillets($billets),$res);
				}
			} else {
				$res = '<div class=error><b>YOU DON\'T HAVE THE NECESSARY RIGHTS.</b></div>';
				$a->affichePage($a->formulaireBillet($billets,$categories),"","",$res);
			}
		} else {
			switch($_GET['attr']) {
				case "body": echo $billet->getAttr("body"); break;
				case "title" : echo $billet->getAttr("titre"); break;
				case "auteur" : echo $billet->getAttr("auteur"); break;
				case "cat" : echo $billet->getAttr("cat"); break;
				default;
			}
		}
	}


	/**
		* Method which display forms for modification of the categories
		*/
	public function formulaireModificationCategorie(){
		$a = new Affichage();
		$categories = Categorie::findAll();
		$billets = Billet::findAll();
		if ($_SESSION['level'] > 1)  {
			if ($_REQUEST['id'] != ''){
				$categ = Categorie::findById($_REQUEST['id']);
				$a->affichePage($a->formulaireCategorieModifier($categ),$a->listeTitresCategories($categories),$a->listeTitresBillets($billets));
			}else{
				$res = '<div class=error><b>NO CATEGORY</b></div>';
				$a->affichePage($a->formulaireCategorie($categories),$a->listeTitresCategories($categories),$a->listeTitresBillets($billets),$res);
			}
		} else {
			$res = '<div class=error><b>YOU DON\'T HAVE THE NECESSARY RIGHTS.</b></div>';
			$a->affichePage($a->formulaireCategorie($categories),"","",$res);
		}
	}

	/**
		* Method which modficate a category using the variables in params
		*/
	public function modificationCategorie(){
		$a = new Affichage();
		$categories = Categorie::findAll();
		$billets = Billet::findAll();
			
		$categ = Categorie::findById($_REQUEST['id']);
		if ($_SESSION['level'] > 1)  {
			if (($_REQUEST['titre'] != '') && ($_REQUEST['description'] != '')){
				$categ->setAttr('titre',$_REQUEST['titre']);
				$categ->setAttr('description',$_REQUEST['description']);
				$categ->update();
					
				$res = '<div class=good><b>CATEGORY EDITED</b></div>';
					
				$users = Utilisateur::findUsers($billets);
					
				$a->affichePage($a->listeBillets($billets),$a->listeTitresCategories($categories),$a->listeTitresBillets($billets),$res);
			}else{

				$res = '<div class=error><b>UNCOMPLETE CATEGORY</b></div>';
				$a->affichePage($a->formulaireCategorie($categories),$a->listeTitresCategories($categories),$a->listeTitresBillets($billets),$res);
			}
		} else {
			$res = '<div class=error><b>YOU DON\'T HAVE THE NECESSARY RIGHTS.</b></div>';
			$a->affichePage($a->formulaireCategorie($categories),"","",$res);
		}
	}

	/**
		* Method which modificate an article using the variables in params
		*/
	public function modificationBillet(){
		$a = new Affichage();
		$categories = Categorie::findAll();
		$billets = Billet::findAll();
		$billet = Billet::findById($_REQUEST['id']);
			
		if (($_SESSION['level'] > 1) || ($_SESSION['id'] != $billet->getAttr('auteur'))){
			if (($_REQUEST['titre'] != '') && ($_REQUEST['body'] != '') && ($_REQUEST['cat_id'] != '')){
				$billet->setAttr('titre',$_REQUEST['titre']);
				$billet->setAttr('auteur',$_SESSION['name']);
				$billet->setAttr('body',$_REQUEST['body']);
				$billet->setAttr('cat_id',$_REQUEST['cat_id']);
				$billet->update();

				$users = Utilisateur::findUsers($billets);
					
				$res = '<div class=good><b>ARTICLE EDITED</b></div>';
				$a->affichePage($a->listeBillets($billets),$a->listeTitresCategories($categories),$a->listeTitresBillets($billets),$res);
			}else{

				$res = '<div class=error><b>UNCOMPLETE ARTICLE</b></div>';
				$a->affichePage($a->formulaireBillet($billets,$categories),$a->listeTitresCategories($categories),$a->listeTitresBillets($billets),$res);
			}
		} else {
			$res = '<div class=error><b>YOU DON\'T HAVE THE NECESSARY RIGHTS.</b></div>';
			$a->affichePage($a->formulaireBillet($billets,$categories),$a->listeTitresCategories($categories),$a->listeTitresBillets($billets),$res);
		}
	}

	/**
	 * 
	 * Method which delete a comment
	 */
	public function removeComment() {
		if ($_SESSION['level'] > 1) {
			$comment = Comment::findById($_GET['id']);
			$comment->delete();
		}
	}

	/**
		* Method which check the login
		*/
	public function login(){
		$a = new Affichage();
		$categories = Categorie::findAll();
		$billets = Billet::findAll();
		$users = Utilisateur::findUsers($billets);
			
		$user = Utilisateur::find($_REQUEST['login'],$_REQUEST['password']);
		if ($user->getAttr('login') != ''){
			$_SESSION['authentification'] = 'correcte';
			$_SESSION['name'] = $_REQUEST['login'];
			$_SESSION['level'] = $user->getAttr('level');
			$_SESSION['mail'] = $user->getAttr('mail');
			$_SESSION['avatar'] = $user->getAttr('avatar');
			$_SESSION['id'] = $user->getAttr('id');

			$res = '<div class=good><b>CONNECTION SUCCEED</b></div>';
		}else{
			$res = '<div class=error><b>CONNECTION FAILED</b></div>';
		}
		$a->affichePage($a->listeBillets($billets),$a->listeTitresCategories($categories),$a->listeTitresBillets($billets),$res);
	}

	public function gestionMembres() {
		$a = new Affichage();
		$categories = Categorie::findAll();
		$billets = Billet::findAll();
		$users = Utilisateur::findAll();
			
		if ($_SESSION['level'] == 3) {
			$a->affichePage($a->gestionMembres($users),'','');
		} else {
			$res = "<div class=error><b>YOU DON\'T HAVE THE NECESSARY RIGHTS.</b></div>";
			$a->affichePage($a->preferences(),'','',$res);
		}
	}


	/**
	 * Method which delete the session
	 */
	public function deconnexion(){
		session_unset();
		session_destroy();
			
		$a = new Affichage();
		$categories = Categorie::findAll();
		$billets = Billet::findAll();
		$users = Utilisateur::findUsers($billets);
			
		$res = '<div class=good><b>LOGED OFF SUCCESSFULLY</b></div>';
		$a->affichePage($a->listeBillets($billets),$a->listeTitresCategories($categories),$a->listeTitresBillets($billets),$res);
	}

	public function administration() {
		$a = new Affichage();
		$categories = Categorie::findAll();
		$billets = Billet::findAll();
		$users = Utilisateur::findAll();
			
		$a->affichePage($a->preferences(),'','');
	}
}
?>
