<?php

require_once 'Affichage.php';

/**
 * Controllor generating displays
 */
class BlogController{

	/**
	 * Method displaying all the articles
	 */
	public function listeAction(){
		$a = new Affichage();
		$billets = Billet::findAll();
		$categories = Categorie::findAll();
		$users = Utilisateur::findUsers($billets);
		$categs = Categorie::findCategs($billets);
		$a->affichePage($a->listeBillets($billets),$a->listeTitresCategories($categories),$a->listeTitresBillets($billets));
	}
		
	/**
	 * Method displaying the details of an article
	 */
	public function detailAction(){
		if(!$_GET['ajax']) {
			$a = new Affichage();
			$billets = Billet::findAll();
			$categories = Categorie::findAll();
			if (isset($_GET['id']) && $_GET['id'] != '' ){
				$billet = Billet::findById($_GET['id']);
				$user = Utilisateur::findById($billet->getAttr('auteur'));
				$categ = Categorie::findById($billet->getAttr('cat_id'));
				$a->affichePage($a->unBillet($billet),$a->listeTitresCategories($categories),$a->listeTitresBillets($billets));
			}else{
				$res = '<div class=error><b>ARTICLE NOT FOUND</b></div>';
				$users = Utilisateur::findUsers($billets);
				$categs = Categorie::findCategs($billets);
				$a->affichePage($a->listeBillets($billets),$a->listeTitresCategories($categories),$a->listeTitresBillets($billets),$res);
			}
		} else {
			if (isset($_GET['id']) && $_GET['id'] != '' ){
				$billet = Billet::findById($_GET['id']);
				echo $billet->getAttr('body');
			}
		}
	}
		
	/**
	 * Method displaying all the articles from a category
	 */
	public function categorieAction(){
		$a = new Affichage();
		$billets = Billet::findAll();
		$categories = Categorie::findAll();
		$users = Utilisateur::findUsers($billets);
		$categs = Categorie::findCategs($billets);
		if (isset($_GET['id']) && $_GET['id'] != ''){
			$billetscat = Billet::findByCat($_GET['id']);
			$a->affichePage($a->listeBillets($billetscat),$a->listeTitresCategories($categories),$a->listeTitresBillets($billets));
		}else{
			$res = '<div class=error><b>CATEGORY NOT FOUND</b></div>';
			$a->affichePage($a->listeBillets($billets),$a->listeTitresCategories($categories),$a->listeTitresBillets($billets),$res);
		}
	}

	/**
	 * Method displaying one user's profile
	 */
	public function profile(){
		$a = new Affichage();
		$billets = Billet::findAll();
		$categories = Categorie::findAll();
		if (isset($_GET['id']) && $_GET['id'] != ''){
			$user = Utilisateur::findById($_GET['id']);
			$a->affichePage($a->ficheUtilisateur($user),$a->listeTitresCategories($categories),$a->listeTitresBillets($billets));
		}else{
			$res = '<div class=error><b>USER NOT FOUND</b></div>';
			$users = Utilisateur::findUsers($billets);
			$categs = Categorie::findCategs($billets);
			$a->affichePage($a->listeBillets($billets),$a->listeTitresCategories($categories),$a->listeTitresBillets($billets),$res);
		}
	}

	/**
	 * 
	 * Method allowing the visitor to write a comment to an article
	 */
	public function addComment() {
		if (($_POST['user'] != '') && ($_POST['body'] != '') && ($_POST['article'] != '')){
			$comment = new Comment();
			$comment->setAttr('user',$_POST['user']);
			$comment->setAttr('body',$_POST['body']);
			$comment->setAttr('article',$_POST['article']);
			$comment->insert();
			$res = '<div class=good><b>COMMENT ADDED</b></div>';
		}else{
			$res = '<div class=error><b>UNCOMPLETE COMMENT</b></div>';
		}
			
		if ($_GET['ajax'] != 1) {
			$a = new Affichage();
			$categories = Categorie::findAll();
			$billets = Billet::findAll();
			$a->affichePage($a->formulaireBillet($billets,$categories),$a->listeTitresAdmin($categories),"",$res);
		} else {
			echo $comment->getAttr('id');
		}
	}
}
?>
